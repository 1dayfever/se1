package se1;

public class Study_Activity{
    private final String name;
    private String description;
    private final double duration; // recorded in hours
    
    public Study_Activity(String name, double duration){
        this.name = name;
        this.duration = duration;
    }
    
    public void setDescription(String a){
        this.description = a;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getDescription(){
        return this.description;
    }
    
    public double getDuration(){
        return this.duration;
    }
    
    //Print out milestone
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Study activity name: ").append(this.name.toUpperCase());
        str.append("\nDescription: ").append(this.description);
        str.append("\nDuration: ").append(this.duration).append(" hours");
        return str.toString();
    }
}