package se1;

import java.util.*;

public class Exam{
    private final String name;
    private Date examDate;
    private final int minutes;
    private Milestone milestone = null;
    private final List<Study_Task>st = new ArrayList<>();
    private final List<Study_Activity>sa = new ArrayList<>();
    
    public Exam(String name, Date examDate, int minutes){
        this.name = name;
        this.examDate = examDate;
        this.minutes = minutes;
    }
    
    public Milestone getMs(){
        return this.milestone;
    }
    
    public List<Study_Activity>getSa(){
        return this.sa;
    }
    
    public List<Study_Task>getSt(){
        return this.st;
    }
    
    public String getName(){
        return this.name;
    }
    
    public int getMinutes(){
        return this.minutes;
    }
    
    public Date getDate(){
        return this.examDate;
    }
    
    public void setDate(Date a){
        this.examDate = a;
    }
    
    public void setMs(Milestone a){
        this.milestone = a;
    }
    
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Name: ").append(this.name);
        str.append("\nDate: ").append(Database.df.format(this.examDate));
        str.append("\nMinutes: ").append(this.minutes).append(" minutes");
        return str.toString();
    }
}