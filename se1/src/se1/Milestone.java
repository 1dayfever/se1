package se1;

import java.util.*;

public class Milestone{
    private String name;
    private Date startDate;
    private Date endDate;
    private String description;
    private boolean status = false; //Default as false, will be true once milestone has been finished
    private Date completedDate = null; //Default as null, will be updated once milestone has been finished
    private double duration = 0; // recorded in hours
    private final List<Study_Activity>sa = new ArrayList<>();
    public enum Type{TASK, EXAM};
    private final Type type;
    
    //Constructor to create a milestone
    public Milestone(String name, Date startDate, Date endDate, Type type){
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
    }
    
    public String getName(){
        return this.name;
    }
    
    public Date getStartDate(){
        return this.startDate;
    }
    
    public Date getEndDate(){
        return this.endDate;
    }
    
    public Type getType(){
        return this.type;
    }
    
    public String getDescription(){
        return this.description;
    }
    
    public boolean getStatus(){
        return this.status;
    }
    
    public Date getCompletedDate(){
        if(this.completedDate == null)
            System.out.println("Task has not been completed");
        return this.completedDate;
    }
    
    public double getDuration(){
        if(this.duration == 0)
            System.out.println("Task has not been completed");
        return this.duration;
    }
    
    public List<Study_Activity>getSa(){
        return this.sa;
    }
    
    public void setStatus(boolean completed){
        this.status = completed;
    }
    
    public void setCompletedDate(Date date){
        this.completedDate = date;
    }
    
    public void setName(String a){
        this.name = a;
    }
    
    public void setStartdate(Date a){
        this.startDate = a;
    }
    
    public void setEnddate(Date a){
        this.endDate = a;
    }
    
    public void setDuration(double time){
        this.duration += time;
    }
    
    public void setDescription(String a){
        this.description = a;
    }
    
    //Print out milestone
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Milestone name: ").append(this.name.toUpperCase());
        str.append("\nStart date: ").append(Database.df.format(this.startDate));
        str.append("\nEnd date: ").append(Database.df.format(this.endDate));
        if(this.description !=null)
            str.append("\nDescription: ").append(this.description);
        else
            str.append("\nDescritption: no description for this milestone yet");
        str.append("\nCompletion status: ").append(this.status);
        if(this.status){
            str.append("\nCompleted date: ").append(Database.df.format(this.completedDate));
            str.append("\nDuration: ").append(this.duration).append(" days");
        }
        return str.toString();
    }
}