package se1;

import java.util.*;

public class Module{
    private String moduleName;
    private String year;
    private int numOfHours;
    private Date startDate;
    private Date endDate;
    private final List<Task>task = new ArrayList<>(); //List to store task of this module
    private final List<Exam>exam = new ArrayList<>();
    private String newsFeed;
    
    //Constuctor to create a module
    public Module(String moduleName, String year, int numOfHours, Date startDate, Date endDate){
        this.moduleName = moduleName;
        this.year = year;
        this.numOfHours = numOfHours;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    public String getName(){
        return this.moduleName;
    }
    
    public String getYear(){
        return this.year;
    }
    
    public int getHours(){
        return this.numOfHours;
    }
    
    public Date getStartDate(){
        return this.startDate;
    }
    
    public Date getEndDate(){
        return this.endDate;
    }
    
    public List<Task> getTask(){
        return this.task;
    }
    
    public List<Exam>getExam(){
        return this.exam;
    }
    
    public String getNewsFeed(){
        if(this.newsFeed == null || this.newsFeed.equals("no"))
            return "No news feed for this module yet";
        else
            return this.newsFeed;
    }
    
    public void updateName(String a){
        this.moduleName = a;
    }
    
    public void updateYear(String a){
        this.year = a;
    }
    
    public void updateStartDate(Date a){
        this.startDate = a;
    }
    
    public void updateEndDate(Date a){
        this.endDate = a;
    }
    
    public void updateNumOfHours(int a){
        this.numOfHours = a;
    }
    
    //Delete a particular task from a module
    public boolean deleteTask(Task a){
        Iterator<Task>it = task.iterator();
        while(it.hasNext()){
            if(it.next().getName().equals(a.getName())){
                it.remove();
                return true;
            }
        }
        return false;
    }
    
    //Loop through the task list and look for a particular task
    public Task findTask(String a){
        Iterator<Task>it = task.iterator();
        while(it.hasNext()){
            Task temp = it.next();
            if(temp.getName().toUpperCase().equals(a.toUpperCase()))
                return temp;
        }
        return null;
    }
    
    //Loop through the exam list and look for a particular exam
    public Exam findExam(String a){
        Iterator<Exam>it = exam.iterator();
        while(it.hasNext()){
            Exam temp = it.next();
            if(temp.getName().toUpperCase().equals(a.toUpperCase()))
                return temp;
        }
        return null;
    }
    
    public void createNewsFeed(String a){
        this.newsFeed = a;
    }
    
    //Print out module
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Module name: ").append(this.moduleName.toUpperCase());
        str.append("\nThis is a year ").append(this.year).append(" module");
        str.append("\nStart date: ").append(Database.df.format(this.startDate));
        str.append("\nEnd date: ").append(Database.df.format(this.endDate));
        str.append("\nNumber of hours: ").append(this.numOfHours);
        str.append("\nNews feed: ").append(this.getNewsFeed());
        str.append("\nTasks: ");
        
        if(task.isEmpty()){
            str.append("No task for this module yet");
            return str.toString();
        }else
            for (Task task1 : task) 
                str.append(task1.getName().toUpperCase()).append(", ");
        return str.toString().substring(0, str.toString().length()-2);
    }
}