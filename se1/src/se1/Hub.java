package se1;

import java.util.*;
import java.text.*;
import java.io.*;

public class Hub extends Database{

    //Lists to store data
    private final List<Students>student = new ArrayList<>();
    private final List<Module>module = new ArrayList<>(); 
    
    //Contstuctor
    public Hub(String name, String pw, String email, accType ac){
        this.name = name;
        this.pw = pw;
        this.email = email;
        this.ac = ac;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getPw(){
        return this.pw;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public accType getAc(){
        return this.ac;
    }
    
    //From super class
    public List<Module> getModules(){
        return this.module;
    }
    
    public List<Students> getStudents(){
        return this.student;
    }
    
    //Loop through the list to make sure not adding duplicated Module to the list
    public void addAllModules(List<Module>a){
        Iterator<Module>it = a.iterator();
        while(it.hasNext()){
            Module temp = it.next();
            Module m = findHubModule(temp.getName());
            if(m == null)
                module.add(temp);
        }
    }
    
    //Loop through the list to remove all modules that are found from the list
    public void removeAllModules(List<Module>a){
        Iterator<Module>it = a.iterator();
        while(it.hasNext()){
            Module temp = it.next();
            Module m =findHubModule(temp.getName());
            if(m!=null)
                module.remove(m);
        }
    }
    
    //Find a particular student from the list
    public Students findHubStudent(String a){
        Iterator<Students>it = student.iterator();
        while(it.hasNext()){
            Students temp = it.next();
            if(temp.getName().equals(a))
                return temp;
        }
        return null;
    }
    
    //Find a particular module from the list
    public Module findHubModule(String a){
        Iterator<Module>it = module.iterator();
        while(it.hasNext()){
            Module m = it.next();
            if(m.getName().toUpperCase().equals(a.toUpperCase()))
                return m;
        }
        return null;
    }
    
    //For student applying extension, the deadline of task c will be postponed for a week
    public static void setDeadline(Students student, Module b, Task c, Exam d) throws ParseException{
        if(c != null && d == null){
            Iterator<Task>it = b.getTask().iterator();
            while(it.hasNext()){
                Task temp = it.next();
                if(temp.getName().equals(c.getName())){
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(temp.getEndDate());
                    cal.add(Calendar.DAY_OF_MONTH, 7);
                    temp.updateEndDate(cal.getTime());
                    updateDeadline(student, b.getName(), c.getName(), cal.getTime());
                    System.out.println("The new deadline for this task is: "+df.format(cal.getTime()));
                }
            }
        }else if(c == null && d!=null){
            Iterator<Exam>it = b.getExam().iterator();
            while(it.hasNext()){
                Exam temp = it.next();
                if(temp.getName().equals(d.getName())){
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(temp.getDate());
                    cal.add(Calendar.DAY_OF_MONTH, 7);
                    temp.setDate(cal.getTime());
                    updateDeadline(student, b.getName(), d.getName(), cal.getTime());
                    System.out.println("The new deadline for this exam is: "+df.format(cal.getTime()));
                }
            }
        }
    }
    
    //Edit the studentdata.txt after an extension has been granted
    public static void updateDeadline(Students student, String b, String c, Date d){
        try{
            File f = Database.getStudentFile();
            File temp = File.createTempFile("file", ".txt", f.getParentFile());
            FileOutputStream fos = new FileOutputStream(temp);
            
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                FileReader fr = new FileReader(f);
                try (BufferedReader br = new BufferedReader(fr)) {
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        if(data[0].equals(student.number) && data[10].equals("0"))
                            bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]+";"+data[8]+";"
                                +data[9]+";"+(Integer.parseInt(data[10])+1)+";"+b+";"+c+";"+df.format(d));
                        else if(data[0].equals(student.number)&& !data[10].equals("0"))
                            bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]+";"+data[8]+";"
                                +data[9]+";"+(Integer.parseInt(data[10])+1)+";"+data[11]+","+b+";"+data[12]+","+c
                                +";"+data[13]+","+df.format(d));
                        else if(Integer.parseInt(data[10]) >0)
                            bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]+";"
                            +data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                        else
                            bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]+";"
                            +data[8]+";"+data[9]+";"+data[10]);
                        bw.newLine();
                    }
                    br.close();
                }
                bw.close();
                f.delete();
                temp.renameTo(f);
            }
        }catch(IOException | NumberFormatException e){
        }
    }
    
    //Find the required SDF from the directory and return it
    public static File[] getSDF(String a){
        File dir = Database.getDir();
        return dir.listFiles((File dir1, String filename) -> filename.endsWith(a+"0.txt") || filename.endsWith(a+"1.txt"));
    }
    
    //Loop through the module arraylist and print out all elements
    public void printModuleList(Hub hub){
        if(module.isEmpty())
            System.out.println("No modules yet");
        else{
            Iterator<Module>it = module.iterator();
            while(it.hasNext())
                System.out.println("\n"+it.next());
        }
    }
    
    //Loop through the student arraylist and print out all elements
    public void printStudentList(Hub hub){
        if(student.isEmpty())
            System.out.println("No students yet");
        else{
            Iterator<Students>it = student.iterator();
            while(it.hasNext())
                System.out.println("\n"+it.next());
        }
    }
    
    //Print out hub's detail
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Account type: ").append(this.ac);
        str.append("\nHub name: ").append(this.name);
        str.append("\nEmail: ").append(this.email);
        return str.toString();
    }
}