package se1;

import java.text.*;
import java.util.*;
import java.io.*;

public class Students extends Database{
    List<String> ganttChart = new ArrayList<>(); //Gantt Chart can be more than one, not sure the GanttChart type so it will be modified
    private boolean upload = false; //state that student has never uploaded a SDF
    private File[] files = null;
    private int count = 0; //How many extension has been applied...
    
    //Will be loaded from studentdata.txt
    private String school;
    private String course;
    private String year;
    private Date semStartDate;
    private Date semEndDate;
    private final List<Module>module = new ArrayList<>(); //List to store student's modules
    private final List<Task>task = new ArrayList<>();
    private final List<Exam>exam = new ArrayList<>();
    private final List<Study_Task>st = new ArrayList<>();
    private final List<Study_Activity>sa = new ArrayList<>();
    private final List<Milestone>milestone = new ArrayList<>();
    
    //Constructor to create new student account
    public Students(String name, String pw, String number, String email, accType ac){
        this.name = name;
        this.pw = pw;
        this.number = number;
        this.email = email;
        this.ac = ac;
    }
    
    //Add sdf into ArrayList and create a Gantt Chart for the file if it can be opened
    public void uploadSemasterDataFile(File f) throws ParseException{
        try{
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            
            module.clear();
            milestone.clear();
            
            if(br.ready()){
                for(String temp = br.readLine(); temp != null; temp = br.readLine()){
                    String[]data =temp.split(";"); //Used the semicolon as delimiter
                    school = data[0];
                    course = data[1];
                    year = data[2];
                    semStartDate = Database.df.parse(data[3]);
                    semEndDate = Database.df.parse(data[4]);

                    String[]mm = data[5].split(",");
                    for(String a: mm){
                        Module m = Database.findModule(a);
                        if(m!=null)
                            module.add(m); 
                    }
                    
                    count = Integer.parseInt(data[9]);
                    String result = number+";"+data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5].toUpperCase()+";"
                            +data[6]+";"+data[7]+";"+data[8]+";"+data[9];
                    writeStudentsData(result);
                }
            }
            br.close();
            upload = true;
        }catch(IOException e){
        }
    }
    
    public static void writeStudentsData(String result){
        try{
            File file = Database.getStudentFile();
            File temp = File.createTempFile("file", ".txt", file.getParentFile());
            FileOutputStream fos = new FileOutputStream(temp);
            
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                FileReader fr = new FileReader(file);
                try (BufferedReader br = new BufferedReader(fr)) {
                    String [] check = result.split(";");
                    
                    String str;
                    boolean found = false;
                    while((str=br.readLine())!=null){
                        String [] data = str.split(";");
                        if(data[0].equals(check[0])){
                            bw.write(result);
                            found = true;
                        }else
                            bw.write(str);
                        bw.newLine();
                    }
                    System.out.println(result);
                    if(!found){
                        bw.write(result);
                        bw.newLine();
                    }
                    br.close();
                }
            bw.close();
            file.delete();
            temp.renameTo(file);
            }
        }catch(Exception e){
        }
    }
    
    //Guess i dont need to explain these...
    public String getName(){
        return this.name;
    }
    
    public String getPw(){
        return this.pw;
    }
    
    public String getSchool(){
        return this.school;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public String getNumber(){
        return this.number;
    }
    
    public String getCourse(){
        return this.course;
    }
    
    public String getYear(){
        return this.year;
    }
    
    public accType getAc(){
        return this.ac;
    }
    
    public File[] getFiles(){
        return this.files;
    }
    
    public int getCount(){
        return this.count;
    }
    
    public boolean getUpload(){
        return this.upload;
    }
    
    public List<Module>getModule(){
        return this.module;
    }
    
    public List<Task> getTask(){
        return this.task;
    }
    
    public List<Milestone>getMilestone(){
        return this.milestone;
    }
    
    public List<Exam>getExam(){
        return this.exam;
    }
    
    public List<Study_Task>getSt(){
        return this.st;
    }
    
    public List<Study_Activity>getSa(){
        return this.sa;
    }
    
    //Student can apply for extension only if count is equal to 0
    public void applyExtension(Module b, Task c, Exam d) throws ParseException{
        Hub.setDeadline(this, b, c, d);
        count++;
    }
    
    //Edit text file to create a new milestone
    public void createMilestone(Milestone ms, String b){
        Functions.writeMilestone(ms, b);
        Functions.editStudentdata(this, ms.getName(), 0);
    }
    
    //Edit text file to create a new study task
    public void createST(Study_Task st, String b){
        Functions.writeST(st, b);
        Functions.editStudentdata(this, st.getName(), 1);
    }
    
    public void createSA(Study_Activity sa, String b){
        Functions.writeSA(sa, b);
        Functions.editStudentdata(this, sa.getName(), 2);
    }
    
    public void setCourse(String a){
        this.course = a;
    }
    
    public void setSchool(String a){
        this.school = a;
    }
    
    public void setYear(String a){
        this.year = a;
    }
    
    public void setCount(int a){
        this.count = a;
    }
    
    public void setUpload(boolean a){
        this.upload = a;
    }
    
    public void setSemStartDate(Date a){
        this.semStartDate = a;
    }
    
    public void setSemEndDate(Date a){
        this.semEndDate = a;
    }
    
    public void setFiles(File[]a){
        this.files = a;
    }
    
    public void setNumber(String a){
        this.number = a;
    }
    
    ////Loop through all the module, task, and milestone arraylists and print out elements
    public void printStudyProfile(){
        if(!upload)
            System.out.println("Please upload your semaster data file first");
        else{
            System.out.println("\nStudy profile: \n");
            System.out.println("Name: "+this.name);
            System.out.println("Number: "+this.number);
            System.out.println("School: "+this.school);
            
            System.out.println("\nModules: ");
            Iterator<Module>it = this.module.iterator();
            while(it.hasNext()){
                System.out.print(it.next().getName().toUpperCase()+"     ");
            }
            System.out.println();
            
            int totaltask = 0, totalms = 0, ftask = 0, fms = 0;
            
            System.out.println("\nTasks: ");
            if(task.isEmpty())
                System.out.println("No tasks");
            
            if(!task.isEmpty()){
                Iterator<Task>itt = this.task.iterator();
                while(itt.hasNext()){
                    System.out.print(itt.next().getName().toUpperCase()+"(not completed)     ");
                    totaltask++;
                }
                System.out.println();
            }
            
            System.out.println("\nMilestones: ");
            if(milestone.isEmpty())
                System.out.println("No milestones");
            
            if(!milestone.isEmpty()){
                Iterator<Milestone>iitt = this.milestone.iterator();
                while(iitt.hasNext()){
                    System.out.print(iitt.next().getName().toUpperCase()+"(not completed)     ");
                    totalms++;
                }
                System.out.println();
            }
            
            int tm = totaltask+totalms;
            int ftm = ftask+fms;
            System.out.println("\nYou are now taking "+module.size()+" modules");
            System.out.println("And have completed "+ftask+"/"+totaltask+" tasks, task completion presentage: "+((double)ftask/totaltask)*100+"%");
            System.out.println("Also have completed "+fms+"/"+totalms+" milestones, milestone completion presentage: "+((double)fms/totalms)*100+"%");
            System.out.println("\nTotal task and milestone completion: "+(fms+ftask)+"/"+(totaltask+totalms)+" ("+((double)ftm/tm)*100+"%)");
        }
    }
    
    ////Loop through the task arraylist and print out task that is needed to be submitted soon 
    private void printUpcomingDeadlines(){
        Calendar cal = Calendar.getInstance();
        
        cal.add(Calendar.DAY_OF_MONTH, 21);
        Date comingSoon = cal.getTime();
        
        List<Task>deadline = new ArrayList<>();
        Iterator<Task>it = this.task.iterator();
        while(it.hasNext()){
            Task t = it.next();
            
            Date ed = t.getEndDate();
            cal.setTime(ed);
            cal.add(Calendar.DAY_OF_MONTH, 21);
            Date confirm = cal.getTime();
            
            if(comingSoon.compareTo(ed)>0 && confirm.compareTo(comingSoon) > 0)
                deadline.add(t);
        }
        
        List<Exam>examinations = new ArrayList<>();
        Iterator<Exam>itt = this.exam.iterator();
        while(itt.hasNext()){
            Exam e = itt.next();
            
            Date ed = e.getDate();
            cal.setTime(ed);
            cal.add(Calendar.DAY_OF_MONTH, 21);
            Date confirm = cal.getTime();
            
            if(comingSoon.compareTo(ed)>0 && confirm.compareTo(comingSoon) > 0)
                examinations.add(e);
        }
        
        if(!deadline.isEmpty() && !examinations.isEmpty()){
            System.out.println("************************************************************");
            System.out.println("Task deadline or exam is coming soon...");
            Iterator<Task>list = deadline.iterator();
            while(list.hasNext()){
                Task t = list.next();
                System.out.println("Task name: "+t.getName().toUpperCase()+", deadline: "+Database.df.format(t.getEndDate()));
            }
            Iterator<Exam>listt = examinations.iterator();
            while(listt.hasNext()){
                Exam e = listt.next();
                System.out.println("Exam name: "+e.getName().toUpperCase()+", date: "+Database.df.format(e.getDate()));
            }
            System.out.println("************************************************************");
        }
    }
    
    //Look for a particular module
    public Module findStudentModule(String a){
        Iterator<Module>it = module.iterator();
        while(it.hasNext()){
            Module temp = it.next();
            if(temp.getName().toUpperCase().equals(a.toUpperCase()))
                return temp;
        }
        return null;
    }
    
    //Look for a particular ms
    public Milestone findStudentMS(String a){
        Iterator<Milestone>it = milestone.iterator();
        while(it.hasNext()){
            Milestone temp = it.next();
            if(temp.getName().toUpperCase().equals(a.toUpperCase()))
                return temp;
        }
        return null;
    }
    
    //Look for a particular st
    public Study_Task findStudentSt(String a){
        Iterator<Study_Task>it = st.iterator();
        while(it.hasNext()){
            Study_Task temp = it.next();
            if(temp.getName().toUpperCase().equals(a.toUpperCase()))
                return temp;
        }
        return null;
    }
    
    //Look for a particular task from a module
    public Task findStudentModuleTasks(Module a, String b){
        Task temp = a.findTask(b);
        if(temp != null)
            return temp;
        else
            return null;
    }
    
    //Look for a particular task from a module
    public Exam findStudentModuleExam(Module a, String b){
        Exam temp = a.findExam(b);
        if(temp != null)
            return temp;
        else
            return null;
    }
    
    //Print out student details
    @Override
    public String toString(){
        printUpcomingDeadlines();
        StringBuilder str = new StringBuilder();
        str.append("Account type: ").append(this.ac);
        str.append("\nStudent name: ").append(this.name);
        str.append("\nStudent number: ").append(this.number);
        if(this.school != null && this.course != null && this.year !=null)
            str.append("\nSchool: ").append(this.school.toUpperCase()).append("\nCourse: ").append(this.course.toUpperCase()).append("\nYear: ").append(this.year);
        str.append("\nEmail: ").append(this.email);
        str.append("\nModules: ");
        if(module.isEmpty()){
            str.append("[]");
            return str.toString();
        }else
            for (Module module1 : module) 
                str.append(module1.getName().toUpperCase()).append(", ");
        return str.toString().substring(0, str.toString().length()-2);
    }
}