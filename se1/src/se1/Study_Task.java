package se1;

import java.util.*;

public class Study_Task{
    private String name;
    private String description;
    private Date startdate;
    private Date enddate;
    private boolean status = false; //Default as false, will be true once milestone has been finished
    private boolean parallel;
    private double duration = 0; // recorded in hours
    private double estimatedtimespent;
    public enum Type{PROGRAMMING, WRITING, READING, STUDYING};
    public enum Type1{TASK, EXAM};
    private final List<Study_Activity>sa = new ArrayList<>();
    private final Type type;
    private final Type1 t;
    
    public Study_Task(String name, Type1 t, Type type, double estimatedtimespent, Date startdate, Date enddate, boolean parallel){
        this.name = name;
        this.t = t;
        this.type = type;
        this.estimatedtimespent = estimatedtimespent;
        this.startdate = startdate;
        this.enddate = enddate;
        this.parallel = parallel;
    }
    
    public void setDuration(double a){
        this.duration += a;
        if(duration >= estimatedtimespent){
            status = true;
        }
    }
    
    public void setStatus(boolean a){
        this.status = a;
    }
    
    public void setParallel(boolean a){
        this.parallel = a;
    }
    
    public void setDescription(String a){
        this.description = a;
    }
    
    public void setStartdate(Date a){
        this.startdate = a;
    }
    
    public void setName(String a){
        this.name = a;
    }
    
    public void setEstimatedTimeSpent(double a){
        this.estimatedtimespent = a;
    }
    
    public void setEnddate(Date a){
        this.enddate = a;
    }
    
    public String getName(){
        return this.name;
    }
    
    public Type getType(){
        return this.type;
    }
    
    public boolean getParallel(){
        return this.parallel;
    }
    
    public Date getStartdate(){
        return this.startdate;
    }
    
    public Date getEnddate(){
        return this.enddate;
    }
    
    public double getEstimeatedTimespent(){
        return this.estimatedtimespent;
    }
    
    public boolean getStatus(){
        return this.status;
    }
    
    public Type1 getT(){
        return this.t;
    }
    
    public double getDuration(){
        if(this.duration == 0)
            System.out.println("Task has not been completed");
        return this.duration;
    }
    
    public String getDescription(){
        if(this.description == null)
            return "No description for this study task yet";
        return this.description;
    }
    
    public List<Study_Activity>getSa(){
        return this.sa;
    }
    
    //Print out milestone
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Study task name: ").append(this.name.toUpperCase());
        str.append("\nType: ").append(this.type);
        str.append("\nEstimated time spent: ").append(this.estimatedtimespent);
        str.append("\nStart date: ").append(Database.df.format(this.startdate));
        str.append("\nEnd date: ").append(Database.df.format(this.enddate));
        str.append("\nParallel: ").append(this.parallel);
        str.append("\nDescription: ").append(this.description);
        str.append("\nCompletion status: ").append(this.status);
        if(this.status){
            str.append("\nTotal time spent: ").append(this.duration).append(" hours");
        }
        return str.toString();
    }
}