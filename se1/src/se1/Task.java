package se1;

import java.util.*;

public class Task{
    private String name;
    private Date startDate;
    private Date endDate; //deadline
    private String description;
    private boolean status = false; //Default status is false, will be true once task has been completed
    private Milestone milestone = null; //List to store milestone of this task
    private final List<Study_Task>st = new ArrayList<>();
    private Date completedDate = null;//Default as null since the task has not been completed
    private int duration = 0; //Time spent on completing the task, recoreded in hours
    
    //Constructor to create a task
    public Task(String name, Date startDate, Date endDate){
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    public String getName(){
        return this.name;
    }
    
    public List<Study_Task>getSt(){
        return this.st;
    }
    
    public Date getStartDate(){
        return this.startDate;
    }
    
    public Date getEndDate(){
        return this.endDate;
    }
    
    public String getDescritption(){
        if(this.description == null)
            System.out.println("No description for this task yet");
        return this.description;
    }
    
    public Milestone getMs(){
        return this.milestone;
    }
    
    public boolean getStatus(){
        return this.status;
    }
    
    public Date getCompleteDate(){
        if(this.completedDate == null)
            System.out.println("Task has not been completed");
        return this.completedDate;
    }
    
    public long getDuration(){
        if(this.duration == 0)
            System.out.println("Task has not been completed");
        return this.duration;
    }
    
    public void setCompleteDate(Date date){
        completedDate = date;
    }
    
    public void setStatus(boolean completed){
        status = completed;
    }
    
    public void setDuration(int time){
        duration = time;
    }
    
    public void setDescription(String a){
        description = a;
    }
    
    public void updateEndDate(Date a){
        endDate = a;
    }
    
    public void updateDescription(String a){
        description = a;
    }
    
    public void updateName(String a){
        name = a;
    }
    
    public void updateStartDate(Date a){
        startDate = a;
    }
    
    public void setMilestone(Milestone a){
        this.milestone = a;
    }
    
    //Print out task
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Task name: ").append(this.name.toUpperCase());
        str.append("\nStart date: ").append(Database.df.format(this.startDate));
        str.append("\nEnd date: ").append(Database.df.format(this.endDate));
        if(this.description !=null)
            str.append("\nDescription: ").append(this.description);
        else
            str.append("\nDescription: no description for this task");
        str.append("\nCompletion status: ").append(this.status);
        return str.toString();
    }
}