package se1;

import java.io.File;
import java.util.*;
import java.text.*;

public abstract class Database{
    
    /*
    A user account will contain:
    - a user number as account name
    - a password
    - an account number
    - email
    - ac (account type, stored in enum: STUDENT/ HUB/ MODULE_ORGANIZER)
    */
    protected String name;
    protected String pw;
    protected String number;
    protected String email;
    protected accType ac;
    protected enum accType{STUDENT, HUB, MODULE_ORGANIZER};
    
    /*
    Abstract Database class contains lists for storing all details....
    These lists are basically designed for file input and output
    */
    protected static List<Database>users = new ArrayList<>();
    protected static List<Module>modules = new ArrayList<>();
    protected static List<Task>tasks = new ArrayList<>();
    protected static List<Milestone>milestones = new ArrayList<>();
    protected static List<Exam>exams = new ArrayList<>();
    
    //Date format
    protected static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    
    private final static File userfile = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/data/users.txt");
    private final static File studentfile = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/data/studentsdata.txt");
    private final static File modulefile = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/data/modules.txt");
    private final static File taskfile = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/data/tasks.txt");
    private final static File examfile = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/data/exams.txt");
    private final static File safile = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/data/sa.txt");
    private final static File stfile = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/data/st.txt");
    private final static File msfile = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/data/milestones.txt");
    private final static File dir = new File("C:/Users/cyrusleung/Documents/NetBeansProjects/se1/src/se1/SDF");
    
    ////Loop through the list and look for a particular user
    public static Database findUser(String a){
        Iterator<Database>it = users.iterator();
        while(it.hasNext()){
            Database temp = it.next();
            if(temp.name.equals(a)){
                return temp;
            }
        }
        return null;
    }
    
    //Loop through the list and look for a particular module
    public static Module findModule(String a){
        Iterator<Module>it = modules.iterator();
        while(it.hasNext()){
            Module temp = it.next();
            if(temp.getName().toUpperCase().equals(a.toUpperCase())){
                return temp;
            }
        }
        return null;
    }
    
    //Loop through the list and look for a particular task
    public static Task findTask(String a){
        Iterator<Task>it = tasks.iterator();
        while(it.hasNext()){
            Task temp = it.next();
            if(temp.getName().toUpperCase().equals(a.toUpperCase())){
                return temp;
            }
        }
        return null;
    }
    
    //Loop through the list and look for a particular task
    public static Exam findExam(String a){
        Iterator<Exam>it = exams.iterator();
        while(it.hasNext()){
            Exam temp = it.next();
            if(temp.getName().toUpperCase().equals(a.toUpperCase())){
                return temp;
            }
        }
        return null;
    }
    
    public static File getUserFile(){
        return userfile;
    }
    
    public static File getModuleFile(){
        return modulefile;
    }
    
    public static File getTaskFile(){
        return taskfile;
    }
    
    public static File getExamFile(){
        return examfile;
    }
    
    public static File getMsFile(){
        return msfile;
    }
    
    public static File getSaFile(){
        return safile;
    }
    
    public static File getStFile(){
        return stfile;
    }
    
    public static File getStudentFile(){
        return studentfile;
    }
    
    public static File getDir(){
        return dir;
    }
    
    public void setPw(String a){
        this.pw = a;
    }
    
    public void setEmail(String a){
        this.email = a;
    }
}