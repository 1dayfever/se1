package se1;

import java.util.*;
import java.text.*;
import java.io.*;

public class Student_Functions{
    private static final Scanner scan = new Scanner (System.in);
    
    //Basic menu
    public static void Student_SelectFunctions(Students student) throws ParseException{
    System.out.println("\nThere are 11 functions for you to select...");

    System.out.println("\nEnter 1 for uploading/getting semaster data file, 2 for updating details, 3 for viewing your information, "
            + "4 for applying extension, \n\t5 to create/modify study task/milestone, 6 to log out...");
    String a = scan.next();

    //Check input, input must be in range
    while(!a.equals("1") && !a.equals("2") && !a.equals("3") && !a.equals("4") && !a.equals("5") &&!a.equals("6")){
        System.out.println("Invalid input, please enter again...");
        a = scan.next();
    }

    //Case when a == 1
    if(a.equals("1")){
        System.out.println("\nEnter 1 to upload semaster data file, 2 to get semaster data file from the hub, or 3 to go back...");
        a = scan.next();

        //Check inputa again
        while(!a.equals("1") && !a.equals("2") && !a.equals("3")){
            System.out.println("Invalid input, please enter again...");
            a = scan.next();
        }

        switch(a){
            case "1":
                Student_UploadSDF(student);
                Student_SelectFunctions(student);
                break;
            case "2":
                Student_getSDF(student);
                Student_SelectFunctions(student);
                break;
            case "3":
                Student_SelectFunctions(student);
                break;
        }
    }
    //Case when a == 2
    else if(a.equals("2")){
        System.out.println("\nEnter 1 to update password, 2 to update email, or 3 to go back...");
        a = scan.next();

        //Check input again
        while(!a.equals("1") && !a.equals("2") && !a.equals("3")){
            System.out.println("Invalid input, please enter again...");
            a = scan.next();
        }

        switch(a){
            case "1":
                Functions.UpdatePw(student);
                Student_SelectFunctions(student);
                break;
            case "2":
                Functions.UpdateEmail(student);
                Student_SelectFunctions(student);
                break;
            case "3":
                Student_SelectFunctions(student);
                break;
        }
    }
    //Case when a == 3
    else if(a.equals("3")){
        System.out.println("\nEnter 1 to view your personal information, 2 to view module details, 3 to view study profile, or 4 to go back...");
        a = scan.next();

        //Check input again
        while(!a.equals("1") && !a.equals("2") && !a.equals("3") && !a.equals("4")){
            System.out.println("Invalid input, please enter again...");
            a = scan.next();
        }

        switch(a){
            case "1":
                System.out.println(student);
                Student_SelectFunctions(student);
                break;
            case "2":
                Student_printModules(student);
                Student_SelectFunctions(student);
            case "3":
                student.printStudyProfile();
                Student_SelectFunctions(student);
                break;
            case "4":
                Student_SelectFunctions(student);
                break;
        }
    }
    //Case when a == 4, only happens when student has already uploaded their SDF
    else if(a.equals("4")){
        if(!student.getUpload()){
            System.out.println("Please upload your semaster data file first");
            Student_SelectFunctions(student);
        }
        Student_ApplyExtension(student);
        Student_SelectFunctions(student);
    }
    //Case when a == 5
    else if(a.equals("5")){
        if(!student.getUpload()){
            System.out.println("Please upload your semaster data file first");
            Student_SelectFunctions(student);
        }
        
        System.out.println("\nEnter 1 to define study task, 2 to define milestone, 3 to create study activity, "
                + "\n\t4 to modify defined study task/milestone, or 5 to go back...");
        a = scan.next();

        //Check input
        while(!a.equals("1") && !a.equals("2") && !a.equals("3") && !a.equals("4") && !a.equals("5")){
            System.out.println("Invalid input, please enter again...");
            a = scan.next();
        }

        switch(a){
            case "1":
                Student_CreateST(student);
                Student_SelectFunctions(student);
                break;
            case "2":
                Student_CreateMS(student);
                Student_SelectFunctions(student);
                break;
            case "3":
                Student_CreateSA(student);
                Student_SelectFunctions(student);
            case "4":
                Student_ModifyData(student);
                Student_SelectFunctions(student);
            case "5":
                Student_SelectFunctions(student);
                break;
        }
    }else{
        //Case when a ==6, log out
        System.out.println();
        Functions.Start();
    }
}

    //Ask user to enter required data for creating a new milestone
    public static void Student_CreateMS(Students student) throws ParseException{
        System.out.println("\nDefining a milestone for a particular task/exam...");
        
        Calendar cal = Calendar.getInstance();
        Functions.printModuleTaskExam(student.getModule(), null,null,null,null);

        System.out.println("Enter the related module: ");
        String space = scan.nextLine();
        String mm = scan.nextLine();

        //Find module
        Module m = student.findStudentModule(mm);

        //Make sure Module m must be found
        while(m==null){
            System.out.println("Please enter a correct module name: ");
            mm = scan.nextLine();
            m = student.findStudentModule(mm);
        }

        //Case when Module m has 0 task and exam
        if(m.getTask().isEmpty() && m.getExam().isEmpty()){
            System.out.println("This module has 0 task and exam, therefore no need to define a milestone");
        }else{
            System.out.println("Enter 1 to define a milestone for a task, 2 to define a milestone for an exam, or 3 to go back: ");
            String ans = scan.nextLine();

            while(!ans.equals("1") && !ans.equals("2") && !ans.equals("3")){
                System.out.println("Invalid input, please try again: ");
                ans = scan.nextLine();
            }
            
            if(ans.equals("1")){
                if(m.getTask().isEmpty())
                    System.out.println("No tasks for this module...");
                else{
                    Functions.printModuleTaskExam(null, m.getTask(),null,null,null);

                    System.out.println("Enter the related task: ");
                    String tk = scan.nextLine();

                    //Find task
                    Task t = m.findTask(tk);
                    //Make sure Task t must be found from the module's task list
                    while(t==null){
                        System.out.println("Please enter a correct task name: ");
                        tk = scan.nextLine();
                        t = m.findTask(tk);
                    }

                    if(t.getEndDate().compareTo(cal.getTime()) <0){
                        System.out.println("The deadline of this task has already passed, the end date of this task is: "+t.getEndDate());
                    }else if(t.getMs() != null){
                        System.out.println("A milestone for this task has already been defined");
                    }else{
                        //New milestone's name
                        System.out.println("\nEnter milestone name: ");
                        String name = scan.nextLine();

                        while(name.equals("") || name.equals(" ")){
                            System.out.println("Milestone name can not be empty, please retry: ");
                            name = scan.nextLine();
                        }
                        
                        //Print out task's start date and end date
                        //New milestone's start date and end date must be within task's start date and end date range
                        System.out.println("\nStart date and end date for this task is: "+Database.df.format(t.getStartDate())
                                +" and "+Database.df.format(t.getEndDate()));

                        //New milestone's start date
                        System.out.println("Enter milestone start date in a format of dd/mm/yyyy: ");
                        String sd = scan.nextLine();
                        Date startdate = Database.df.parse(sd);

                        //New milestone's end date
                        System.out.println("Enter milestone end date in a format of dd/mm/yyyy: ");
                        String ed = scan.nextLine();
                        Date enddate = Database.df.parse(ed);

                        //Compare start date and end date, start date cannot be >= end date, and must be in range
                        while(!Functions.checkDate(t.getStartDate(), t.getEndDate(), startdate, enddate)){
                            System.out.println("\nEnter start date in a format of dd/mm/yyyy: ");
                            sd = scan.nextLine();
                            startdate = Database.df.parse(sd);
                            System.out.println("Enter end date in a format of dd/mm/yyyy: ");
                            ed = scan.nextLine();
                            enddate = Database.df.parse(ed);
                            Functions.checkDate(t.getStartDate(), t.getEndDate(), startdate,enddate);
                        }

                        //New milestone's description
                        System.out.println("\nEnter milestone description: ");
                        String description = scan.nextLine();

                        //Create new milestone and add it to the database
                        Milestone ms = new Milestone(name, startdate, enddate, Milestone.Type.TASK);
                        ms.setDescription(description);
                        Database.milestones.add(ms);
                        t.setMilestone(ms);
                        student.getMilestone().add(ms);

                        //Create a string for writting it to the text file
                        String milestone = student.number+";"+m.getName()+";"+Milestone.Type.TASK+";"+t.getName()+";"+name+";"+sd+";"+ed+";"+description;

                        //update the text file
                        student.createMilestone(ms, milestone);
                        System.out.println("Milestone for "+mm+": "+tk+" has been created successfully");
                    }
                }
            }else if(ans.equals("2")){
                if(m.getExam().isEmpty())
                    System.out.println("No exam for this module...");
                else{
                    Functions.printModuleTaskExam(null, null, m.getExam(),null,null);

                    System.out.println("Enter the related exam: ");
                    String tk = scan.nextLine();

                    //Find task
                    Exam t = m.findExam(tk);
                    //Make sure Task t must be found from the module's task list
                    while(t==null){
                        System.out.println("Please enter a correct task name: ");
                        tk = scan.nextLine();
                        t = m.findExam(tk);
                    }

                    if(t.getDate().compareTo(cal.getTime()) <0){
                        System.out.println("This exam is already finished, the exam date is: "+t.getDate());
                    }else if(t.getMs() != null){
                        System.out.println("A milestone for this exam has already been defined");
                    }else{
                        //New milestone's name
                        System.out.println("\nEnter milestone name: ");
                        String name = scan.nextLine();

                        while(name.equals("") || name.equals(" ")){
                            System.out.println("Milestone name can not be empty, please retry: ");
                            name = scan.nextLine();
                        }
                        
                        //Print out exam's date
                        //New milestone's start date and end date must be smaller than exam's date
                        System.out.println("\nExam date for this exam is: "+Database.df.format(t.getDate()));

                        //New milestone's start date
                        System.out.println("Enter milestone start date in a format of dd/mm/yyyy: ");
                        String sd = scan.nextLine();
                        Date startdate = Database.df.parse(sd);

                        //New milestone's end date
                        System.out.println("Enter milestone end date in a format of dd/mm/yyyy: ");
                        String ed = scan.nextLine();
                        Date enddate = Database.df.parse(ed);

                        //Compare start date and end date, start date cannot be >= end date, and must be in range
                        while(startdate.compareTo(t.getDate()) >=0 || enddate.compareTo(t.getDate()) >= 0){
                            System.out.println("\nStart date and end date cannot be greater than or equal to exam date...");
                            System.out.println("Enter start date in a format of dd/mm/yyyy: ");
                            sd = scan.nextLine();
                            startdate = Database.df.parse(sd);
                            System.out.println("Enter end date in a format of dd/mm/yyyy: ");
                            ed = scan.nextLine();
                            enddate = Database.df.parse(ed);
                        }

                        //New milestone's description
                        System.out.println("\nEnter milestone description: ");
                        String description = scan.nextLine();

                        //Create new milestone and add it to the database
                        Milestone ms = new Milestone(name, startdate, enddate, Milestone.Type.EXAM);
                        ms.setDescription(description);
                        Database.milestones.add(ms);
                        t.setMs(ms);
                        student.getMilestone().add(ms);

                        //Create a string for writting it to the text file
                        String milestone = student.number+";"+m.getName()+";"+Milestone.Type.EXAM+";"+t.getName()+";"+name+";"+sd+";"+ed+";"+description;

                        //update the text file
                        student.createMilestone(ms, milestone);
                        System.out.println("Milestone for "+mm+": "+tk+" has been created successfully");
                    }
                }
            }else
                Student_SelectFunctions(student);
        }
    }
    
    //Ask user to enter required data for creating a new milestone
    public static void Student_CreateST(Students student) throws ParseException{
        System.out.println("\nCreating a study task for a particular task/exam...");
        
        Calendar cal = Calendar.getInstance();
        Functions.printModuleTaskExam(student.getModule(), null, null,null,null);

        System.out.println("Enter the related module: ");
        String space = scan.nextLine();
        String mm = scan.nextLine();

        //Find module
        Module m = student.findStudentModule(mm);

        //Make sure Module m must be found
        while(m==null){
            System.out.println("Please enter a correct module name: ");
            mm = scan.nextLine();
            m = student.findStudentModule(mm);
        }

        //Case when Module m has 0 task and exam
        if(m.getTask().isEmpty() && m.getExam().isEmpty()){
            System.out.println("This module has 0 task and exam, therefore no need to create a study task");
        }else{
            System.out.println("Enter 1 to create a study task for a task, 2 to create a study task for an exam, or 3 to go back: ");
            String ans = scan.nextLine();

            while(!ans.equals("1") && !ans.equals("2") && !ans.equals("3")){
                System.out.println("Invalid input, please try again: ");
                ans = scan.nextLine();
            }
            
            if(ans.equals("1")){
                if(m.getTask().isEmpty())
                    System.out.println("No tasks for this module...");
                else{
                    Functions.printModuleTaskExam(null, m.getTask(), null,null,null);

                    System.out.println("Enter the related task: ");
                    String tk = scan.nextLine();

                    //Find task
                    Task t = m.findTask(tk);
                    //Make sure Task t must be found from the module's task list
                    while(t==null){
                        System.out.println("Please enter a correct task name: ");
                        tk = scan.nextLine();
                        t = m.findTask(tk);
                    }

                    if(t.getEndDate().compareTo(cal.getTime()) <0){
                        System.out.println("The deadline of this task has already passed, the end date of this task is: "+t.getEndDate());
                    }else{
                        //New milestone's name
                        System.out.println("\nEnter study task name: ");
                        String name = scan.nextLine();
                        
                        while(name.equals("") || name.equals(" ")){
                            System.out.println("Study task name can not be empty, please retry: ");
                            name = scan.nextLine();
                        }

                        System.out.println("\nEnter study task type, it can eiter be programming/reading/writing/studying: ");
                        String temp = scan.nextLine();
                        
                        while(!temp.toUpperCase().equals("PROGRAMMING") && !temp.toUpperCase().equals("READING") 
                                && !temp.toUpperCase().equals("WRITING") && !temp.toUpperCase().equals("STUDYING")){
                            System.out.println("Please enter a valid study type: ");
                            temp = scan.nextLine();
                        }
                        
                        Study_Task.Type type;
                        if(temp.toUpperCase().equals("PROGRAMMING"))
                            type = Study_Task.Type.PROGRAMMING;
                        else if(temp.toUpperCase().equals("READING"))
                            type = Study_Task.Type.READING;
                        else if(temp.toUpperCase().equals("WRITING"))
                            type = Study_Task.Type.WRITING;
                        else
                            type = Study_Task.Type.STUDYING;
                        
                        System.out.println("\nEnter the estimated hours to spend for this study task: ");
                        String hour = scan.nextLine();
                        
                        while(!hour.matches("\\d+")){
                            System.out.println("Please enter a valid hour: ");
                            hour = scan.nextLine();
                        }

                        //New st's start date and end date must be smaller than task's end date
                        System.out.println("\nStart date and end date for this task is: "+Database.df.format(t.getStartDate())+", "+Database.df.format(t.getEndDate()));

                        //New st's start date
                        System.out.println("Enter study task start date in a format of dd/mm/yyyy: ");
                        String sd = scan.nextLine();
                        Date startdate = Database.df.parse(sd);

                        //New st's end date
                        System.out.println("Enter study task end date in a format of dd/mm/yyyy: ");
                        String ed = scan.nextLine();
                        Date enddate = Database.df.parse(ed);

                        //Compare start date and end date, start date cannot be >= end date, and must be in range
                        while(startdate.compareTo(t.getEndDate()) >=0 || enddate.compareTo(t.getEndDate()) >= 0){
                            System.out.println("\nStart date and end date cannot be greater than or equal to task end date...");
                            System.out.println("Enter start date in a format of dd/mm/yyyy: ");
                            sd = scan.nextLine();
                            startdate = Database.df.parse(sd);
                            System.out.println("Enter end date in a format of dd/mm/yyyy: ");
                            ed = scan.nextLine();
                            enddate = Database.df.parse(ed);
                        }
                        
                        System.out.println("\nEnter study task description: ");
                        String description = scan.nextLine();
                        
                        System.out.println("\nEnter 1 to set this study task can be done in parallel: ");
                        String parallel = scan.nextLine();
                        boolean result = parallel.equals("1");
                        
                        //Create new milestone and add it to the database
                        Study_Task st = new Study_Task(name, Study_Task.Type1.TASK, type, Double.parseDouble(hour), startdate, enddate,result);
                        student.getSt().add(st);
                        st.setDescription(description);

                        //Create a string for writting it to the text file
                        String ST = student.number+";"+m.getName()+";"+Study_Task.Type1.TASK+";"+t.getName()+";"+name+";"+hour+";"+type+";"
                                +sd+";"+ed+";"+description+";"+result;

                        //update the text file
                        student.createST(st, ST);
                        System.out.println("Study task for "+mm+": "+tk+" has been created successfully");
                    }
                }
            }else if(ans.equals("2")){
                if(m.getExam().isEmpty())
                    System.out.println("No exam for this module...");
                else{
                    Functions.printModuleTaskExam(null, null, m.getExam(),null,null);

                    System.out.println("Enter the related exam: ");
                    String tk = scan.nextLine();

                    //Find task
                    Exam t = m.findExam(tk);
                    //Make sure Task t must be found from the module's task list
                    while(t==null){
                        System.out.println("Please enter a correct task name: ");
                        tk = scan.nextLine();
                        t = m.findExam(tk);
                    }

                    if(t.getDate().compareTo(cal.getTime()) <0){
                        System.out.println("This exam is already finished, the exam date is: "+t.getDate());
                    }else{
                        //New milestone's name
                        System.out.println("\nEnter study task name: ");
                        String name = scan.nextLine();

                        while(name.equals("") || name.equals(" ")){
                            System.out.println("Study task name can not be empty, please retry: ");
                            name = scan.nextLine();
                        }
                        
                        System.out.println("\nEnter study task type, it can eiter be programming/reading/writing/studying: ");
                        String temp = scan.nextLine();
                        
                        while(!temp.toUpperCase().equals("PROGRAMMING") && !temp.toUpperCase().equals("READING") 
                                && !temp.toUpperCase().equals("WRITING") && !temp.toUpperCase().equals("STUDYING")){
                            System.out.println("Please enter a valid study type: ");
                            temp = scan.nextLine();
                        }
                        
                        Study_Task.Type type;
                        if(temp.toUpperCase().equals("PROGRAMMING"))
                            type = Study_Task.Type.PROGRAMMING;
                        else if(temp.toUpperCase().equals("READING"))
                            type = Study_Task.Type.READING;
                        else if(temp.toUpperCase().equals("WRITING"))
                            type = Study_Task.Type.WRITING;
                        else
                            type = Study_Task.Type.STUDYING;
                        
                        System.out.println("\nEnter the estimated hours to spend for this study task: ");
                        String hour = scan.nextLine();
                        
                        while(!hour.matches("\\d+")){
                            System.out.println("Please enter a valid hour: ");
                            hour = scan.nextLine();
                        }
                        
                        //New st's start date and end date must be smaller than exam's end date
                        System.out.println("\nThe exam date is: "+Database.df.format(t.getDate()));

                        //New st's start date
                        System.out.println("Enter study task start date in a format of dd/mm/yyyy: ");
                        String sd = scan.nextLine();
                        Date startdate = Database.df.parse(sd);

                        //New st's end date
                        System.out.println("Enter study task end date in a format of dd/mm/yyyy: ");
                        String ed = scan.nextLine();
                        Date enddate = Database.df.parse(ed);

                        //Compare start date and end date, start date cannot be >= end date, and must be in range
                        while(startdate.compareTo(t.getDate()) >=0 || enddate.compareTo(t.getDate()) >= 0){
                            System.out.println("\nStart date and end date cannot be greater than or equal to exam date...");
                            System.out.println("Enter start date in a format of dd/mm/yyyy: ");
                            sd = scan.nextLine();
                            startdate = Database.df.parse(sd);
                            System.out.println("Enter end date in a format of dd/mm/yyyy: ");
                            ed = scan.nextLine();
                            enddate = Database.df.parse(ed);
                        }
                        
                        System.out.println("\nEnter study task description: ");
                        String description = scan.nextLine();
                        
                        System.out.println("\nEnter 1 to set this study task cant be done in parallel: ");
                        String parallel = scan.nextLine();
                        boolean result = parallel.equals("1");

                        //Create new milestone and add it to the database
                        Study_Task st = new Study_Task(name, Study_Task.Type1.EXAM, type, Double.parseDouble(hour), startdate, enddate,result);
                        student.getSt().add(st);
                        st.setDescription(description);

                        //Create a string for writting it to the text file
                        String ST = student.number+";"+m.getName()+";"+Study_Task.Type1.EXAM+";"+t.getName()+";"+name+";"+hour+";"+type+";"
                                +sd+";"+ed+";"+description+";"+result;

                        //update the text file
                        student.createST(st, ST);
                        System.out.println("Study task for "+mm+": "+tk+" has been created successfully");
                    }
                }
            }else
                Student_SelectFunctions(student);
        }
    }
    
    //Ask user to enter required data for creating a new milestone
    public static void Student_CreateSA(Students student) throws ParseException{
        System.out.println("\nDefining a study activity for a particular study task/milestone...");
        
        System.out.println("\nEnter study activity name: ");
        String space = scan.nextLine();
        String name = scan.nextLine();

        while(name.equals("") || name.equals(" ")){
            System.out.println("Study activity name can not be empty, please retry: ");
            name = scan.nextLine();
        }
        
        System.out.println("\nEnter hours spent for this study activity: ");
        String hours = scan.nextLine();
        
        while(!hours.matches("\\d+")){
            System.out.println("Please enter a valid hour: ");
            hours = scan.nextLine();
        }
        
        System.out.println("\nEnter study activity description: ");
        String description = scan.nextLine();

        Study_Activity sa = new Study_Activity(name, Double.parseDouble(hours));
        sa.setDescription(description);
        student.getSa().add(sa);
        String SA = "";
        
        System.out.println("\nYou have these study task and milestones...");
        Functions.printModuleTaskExam(null, null,null,null,student.getMilestone());
        Functions.printModuleTaskExam(null, null,null,student.getSt(),null);
        System.out.println("\nWhich study task or milestone you would like to attatch this study activity to? ");
        String temp = scan.nextLine();
        
        Milestone ms = student.findStudentMS(temp);
        Study_Task st = student.findStudentSt(temp);
        
        while(ms==null && st == null){
            System.out.println("Please enter a correct study task or milestone...");
            temp = scan.nextLine();
            ms = student.findStudentMS(temp);
            st = student.findStudentSt(temp);
        }
        
        if(ms==null && st!=null){
            Iterator<Study_Task>it = student.getSt().iterator();
            int count = 0;
            ArrayList<Study_Task>node = new ArrayList<>();
            
            while (it.hasNext()){
                Study_Task stk = it.next();
                if (count > 0 && !stk.getParallel() && !stk.getStatus())
                    node.add(stk);
                count++;
            }
            
            if(st.getParallel() || st.getName().equals(node.get(0).getName())){
                st.setDuration(Double.parseDouble(hours));
                st.getSa().add(sa);
                SA += student.number+";"+name+";"+hours+";"+description+";"+st.getName();
            }else if(!st.getName().equals(node.get(0).getName())){
                System.out.println("You have to finish study task: "+node.get(0).getName()+" first before working on this study task...");
                Student_SelectFunctions(student);
            }
        }else if(ms!=null && st == null){
            ms.setDuration(Double.parseDouble(hours));
            ms.getSa().add(sa);
            SA = student.number+";"+name+";"+hours+";"+description+";"+ms.getName();
        }
        
        System.out.println("\nEnter 1 to attatch this study activity to other study task/milestone?");
        String input = scan.nextLine();
        
        while(input.equals("1")){
            System.out.println("\nWhich study task or milestone you would like to attatch this study activity to? ");
            String ans = scan.nextLine();
            if(ans.equals(temp))
                System.out.println("You have already attatched this study activity");
            else{
                ms = student.findStudentMS(ans);
                st = student.findStudentSt(ans);
                if(ms==null && st==null)
                    System.out.println("Invaild input, no milestone/study task can be found");
                else if(ms != null && st==null){
                    ms.setDuration(Double.parseDouble(hours));
                    ms.getSa().add(sa);
                    SA += ","+ms.getName();
                }else if(st !=null && ms == null){
                    st.setDuration(Double.parseDouble(hours));
                    st.getSa().add(sa);
                    SA += ","+st.getName();
                }
            }
            System.out.println("\nEnter 1 to attatch this study activity to other study task/milestone?");
            input = scan.nextLine();
        }
        
        student.createSA(sa, SA);
        System.out.println("Study activity has been created successfully");
    }
    
    //Not done yet
    public static void Student_ModifyData(Students student) throws ParseException{
        System.out.println("\nEnter 1 to modify milestone, 2 to modify study task, or 3 to go back");
        String space = scan.nextLine();
        String a = scan.nextLine();
        
        while(!a.equals("1") && !a.equals("2") && !a.equals("3")){
            System.out.println("Invalid input, please retry: ");
            a = scan.nextLine();
        }
        
        if(a.equals("1")){
            if(student.getMilestone().isEmpty()){
                System.out.println("\nYou don't have any milestones, please go define one first...");
                Student_SelectFunctions(student);
            }
            System.out.println("\nYou have these milestones...");
            Functions.printModuleTaskExam(null, null,null,null,student.getMilestone());
            
            System.out.println("Enter milestone name: ");
            String mm = scan.nextLine();
            Milestone ms = student.findStudentMS(mm);
            
            if(ms == null){
                System.out.println("Invalid input, please retry");
                Student_SelectFunctions(student);
            }else{
                System.out.println("\nEnter 1 to modify name, 2 to modify start date, 3 to modify end date, 4 to modify description, "
                        + "5 to delete milestone, or 6 to go back...");
                a = scan.nextLine();
                
                while(!a.equals("1") && !a.equals("2") && !a.equals("3") && !a.equals("4") && !a.equals("5") && !a.equals("6")){
                    System.out.println("Invalid input, please retry: ");
                    a = scan.nextLine();
                }
                
                switch(a){
                    case "1":
                        Student_UpdateMsName(student, ms);
                        break;
                    case "2":
                        Student_UpdateMsStartDate(student, ms);
                        break;
                    case "3":
                        Student_UpdateMsEndDate(student, ms);
                        break;
                    case "4":
                        Student_UpdateMsDescription(student, ms);
                        break;
                    case "5":
                        Functions.editMilestonedata(ms, ms.getName(), 4, student);
                        Functions.editStudentdata(student, ms.getName(), 4);
                        
                        Iterator<Milestone>it = student.getMilestone().iterator();
                        while(it.hasNext()){
                            Milestone temp = it.next();
                            if(temp.getName().toUpperCase().equals(ms.getName().toUpperCase()))
                                it.remove();
                        }
                        System.out.println("\nMilestone has been deleted successfully");
                        
                        break;
                    case "6":
                        break;
                }
            }
        }else if(a.equals("2")){
            if(student.getSt().isEmpty()){
                System.out.println("\nYou don't have any study tasks, please go define one first...");
                Student_SelectFunctions(student);
            }
            System.out.println("\nYou have these study task...");
            Functions.printModuleTaskExam(null, null,null,student.getSt(),null);
            
            System.out.println("Enter study task name: ");
            String mm = scan.nextLine();
            Study_Task st = student.findStudentSt(mm);
            
            if(st == null){
                System.out.println("Invalid input, please retry");
                Student_SelectFunctions(student);
            }else{
                System.out.println("\nEnter 1 to modify name, 2 to modify start date, 3 to modify end date, 4 to modify description, "
                        + "5 to modify estimated time spent, \n\t 6 to delete study task or 7 to go back...");
                a = scan.nextLine();
                
                while(!a.equals("1") && !a.equals("2") && !a.equals("3") && !a.equals("4") && !a.equals("5") && !a.equals("6") && !a.equals("7")){
                    System.out.println("Invalid input, please retry: ");
                    a = scan.nextLine();
                }
                
                switch(a){
                    case "1":
                        Student_UpdateStName(student, st);
                        break;
                    case "2":
                        Student_UpdateStStartDate(student, st);
                        break;
                    case "3":
                        Student_UpdateStEndDate(student, st);
                        break;
                    case "4":
                        Student_UpdateStDescription(student, st);
                        break;
                    case "5":
                        Student_UpdateStEstimeatedTimeSpent(student, st);
                        break;
                    case "6":
                        Functions.editStdata(st, st.getName(), 4, student);
                        Functions.editStudentdata(student, st.getName(), 6);
                        
                        Iterator<Study_Task>it = student.getSt().iterator();
                        while(it.hasNext()){
                            Study_Task temp = it.next();
                            if(temp.getName().toUpperCase().equals(st.getName().toUpperCase()))
                                it.remove();
                        }
                        System.out.println("\nStudy task has been deleted successfully");
                        
                        break;
                    case "7":
                        break;
                }
            }
        }else
            Student_SelectFunctions(student);
            
    }
    
    //Change module name
    public static void Student_UpdateMsName(Students student, Milestone m){
        //Module's new name
        System.out.println("Enter new milestone name: ");
        String a = scan.nextLine();
        
        while(a.equals("") || a.equals(" ")){
            System.out.println("Milestone name can not be empty, please retry: ");
            a = scan.nextLine();
        }
        
        Functions.editMilestonedata(m, a, 0, student);
        Functions.editStudentdata(student, a+";"+m.getName(), 3);
        m.setName(a);
        System.out.println("Milestone has been renamed to "+a.toUpperCase());
    }
    
    //Modify the start date of a module
    public static void Student_UpdateMsStartDate(Students student, Milestone m) throws ParseException{
        System.out.println("The current start date is: "+Database.df.format(m.getStartDate())+", and end date is: "+Database.df.format(m.getEndDate()));
        
        //Ms's new start date
        System.out.println("Enter new start date in a format of dd/mm/yyyy: ");
        String a = scan.nextLine();
        
        //Double check
        System.out.println("Enter new start date in a format of dd/mm/yyyy again: ");
        String b = scan.nextLine();
        
        if(a.compareTo(b) == 0){
            Functions.editMilestonedata(m, a, 1, student);
            m.setStartdate(Database.df.parse(a));
            System.out.println(m.getName().toUpperCase()+" start date has been updated to "+a+" successfully");
        }
        //Case when user entered the new start date wrongly...
        else
            System.out.println("Please make sure you have entered the new start date correctly...");
    }
    
    //Modify the end date of a module
    public static void Student_UpdateMsEndDate(Students student, Milestone m) throws ParseException{
        System.out.println("The current start date is: "+Database.df.format(m.getStartDate())+", and end date is: "+Database.df.format(m.getEndDate()));
        
        //Ms's new end date
        System.out.println("Enter new end date in a format of dd/mm/yyyy: ");
        String a = scan.nextLine();
        
        //Double check
        System.out.println("Enter new end date in a format of dd/mm/yyyy again: ");
        String b = scan.nextLine();
        
        if(a.compareTo(b) == 0){
            Functions.editMilestonedata(m, a, 2, student);
            m.setEnddate(Database.df.parse(a));
            System.out.println(m.getName().toUpperCase()+" end date has been updated to "+a+" successfully");
        }
        //Case when user entered the new end date wrongly
        else
            System.out.println("Please make sure you have entered the new start date correctly...");
    }
    
    //Change module name
    public static void Student_UpdateMsDescription(Students student, Milestone m){
        //Module's new name
        System.out.println("Enter new milestone description: ");
        String a = scan.nextLine();
        
        Functions.editMilestonedata(m, a, 3, student);
        m.setDescription(a);
        System.out.println("Milestone description has been updated to "+a.toUpperCase());
    }
    
    public static void Student_UpdateStName(Students student, Study_Task m){
        //Module's new name
        System.out.println("Enter new study task name: ");
        String a = scan.nextLine();
        
        while(a.equals("") || a.equals(" ")){
            System.out.println("Study task name can not be empty, please retry: ");
            a = scan.nextLine();
        }
        
        Functions.editStdata(m, a, 0, student);
        Functions.editStudentdata(student, a+";"+m.getName(), 5);
        m.setName(a);
        System.out.println("Study task has been renamed to "+a.toUpperCase());
    }
    
    //Modify the start date of a st
    public static void Student_UpdateStStartDate(Students student, Study_Task m) throws ParseException{
        System.out.println("The current start date is: "+Database.df.format(m.getStartdate())+", and end date is: "+Database.df.format(m.getEnddate()));
        
        //Ms's new start date
        System.out.println("Enter new start date in a format of dd/mm/yyyy: ");
        String a = scan.nextLine();
        
        //Double check
        System.out.println("Enter new start date in a format of dd/mm/yyyy again: ");
        String b = scan.nextLine();
        
        if(a.compareTo(b) == 0){
            Functions.editStdata(m, a, 1, student);
            m.setStartdate(Database.df.parse(a));
            System.out.println(m.getName().toUpperCase()+" start date has been updated to "+a+" successfully");
        }
        //Case when user entered the new start date wrongly...
        else
            System.out.println("Please make sure you have entered the new start date correctly...");
    }
    
    //Modify the end date of a st
    public static void Student_UpdateStEndDate(Students student, Study_Task m) throws ParseException{
        System.out.println("The current start date is: "+Database.df.format(m.getStartdate())+", and end date is: "+Database.df.format(m.getEnddate()));
        
        //Ms's new end date
        System.out.println("Enter new end date in a format of dd/mm/yyyy: ");
        String a = scan.nextLine();
        
        //Double check
        System.out.println("Enter new end date in a format of dd/mm/yyyy again: ");
        String b = scan.nextLine();
        
        if(a.compareTo(b) == 0){
            Functions.editStdata(m, a, 2, student);
            m.setEnddate(Database.df.parse(a));
            System.out.println(m.getName().toUpperCase()+" end date has been updated to "+a+" successfully");
        }
        //Case when user entered the new end date wrongly
        else
            System.out.println("Please make sure you have entered the new start date correctly...");
    }
    
    //Change st description
    public static void Student_UpdateStDescription(Students student, Study_Task m){
        //Module's new name
        System.out.println("Enter new milestone description: ");
        String a = scan.nextLine();
        
        Functions.editStdata(m, a, 3, student);
        m.setDescription(a);
        System.out.println("Study description description has been updated to "+a.toUpperCase());
    }
    
    public static void Student_UpdateStEstimeatedTimeSpent(Students student, Study_Task st){
        System.out.println("Enter new study task estimated time spent: ");
        String a = scan.nextLine();
        
        while(!a.matches("\\d+")){
            System.out.println("Please enter a valid hour: ");
            a = scan.nextLine();
        }
        
        Functions.editStdata(st, a, 5, student);
        st.setEstimatedTimeSpent(Double.parseDouble(a));
        System.out.println("Study task's estimated time spent has been updated to "+a.toUpperCase());
    }
    
    public static void Student_printModules(Students student) throws ParseException{
        if(!student.getUpload()){
            System.out.println("Please upload your semaster data file first...");
            Student_SelectFunctions(student);
        }
        
        System.out.println("\nPlease select a module to print out: ");
        
        Functions.printModuleTaskExam(student.getModule(), null, null,null,null);

        System.out.println("Enter the related module: ");
        String space = scan.nextLine();
        String mm = scan.nextLine();

        //Find module
        Module m = student.findStudentModule(mm);

        //Make sure Module m must be found
        while(m==null){
            System.out.println("Please enter a correct module name: ");
            mm = scan.nextLine();
            m = student.findStudentModule(mm);
        }
        
        System.out.println("\n"+m);
        
        System.out.println("\nEnter 1 to print out all task details, 2 to print out all exam details, or 3 to go back: ");
        String a = scan.nextLine();
        
        //Check input
        while(!a.equals("1") && !a.equals("2") && !a.equals("3")){
            System.out.println("Invalid input, please enter again...");
            a = scan.next();
        }
        
        switch(a){
            case "1":
                System.out.println("\nTask: ");
                if(m.getTask().isEmpty())
                    System.out.println("No task for module: "+m.getName().toUpperCase()+"\n");
                else{
                    Iterator<Task>task = m.getTask().iterator();
                    while(task.hasNext()){
                        Task t = task.next();
                        System.out.println(t+"\n");
                        /*System.out.println(t+"\n\nMilestones: ");
                        if(student.getMilestone().isEmpty()){
                            System.out.println("No milestones for task: "+t.getName().toUpperCase()+"\n");
                        }else{
                            boolean printed = false;
                            Iterator<Milestone>ms = student.getMilestone().iterator();
                            while(ms.hasNext()){
                                Milestone temp = ms.next();
                                if(temp.getType().equals(Milestone.Type.TASK)){
                                    System.out.println(temp+"\n");
                                    System.out.println("Study activities: ");
                                    if(temp.getSa().isEmpty()){
                                        System.out.println("No study activities for milestone: "+temp.getName().toUpperCase()+"\n");
                                    }else{
                                        Iterator<Study_Activity>sa = temp.getSa().iterator();
                                        while(sa.hasNext()){
                                            System.out.println(sa.next()+"\n");
                                        }
                                    }
                                    printed = true;
                                }
                            }
                            if(!printed)
                                System.out.println("No milestones for task: "+t.getName().toUpperCase()+"\n");
                        }*/
                        
                        System.out.println("Study tasks: ");
                        if(t.getSt().isEmpty()){
                            System.out.println("No study tasks for task: "+t.getName().toUpperCase()+"\n");
                        }else{
                            Iterator<Study_Task>st = t.getSt().iterator();
                            while(st.hasNext()){
                                Study_Task temp = st.next();
                                System.out.println(temp+"\n");
                                System.out.println("Study activities: ");
                                if(temp.getSa().isEmpty()){
                                    System.out.println("No study activities for study task: "+temp.getName().toUpperCase()+"\n");
                                }else{
                                    Iterator<Study_Activity>sa = temp.getSa().iterator();
                                    while(sa.hasNext()){
                                        System.out.println(sa.next()+"\n");
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "2":
                System.out.println("\nExam: ");
                if(m.getExam().isEmpty())
                    System.out.println("No exam for module: "+m.getName().toUpperCase()+"\n");
                else{
                    Iterator<Exam>e = m.getExam().iterator();
                    while(e.hasNext()){
                        Exam exam = e.next();
                        System.out.println(exam+"\n");
                        /*System.out.println(exam+"\n\nMilestones: ");
                        if(exam.getMs().isEmpty()){
                            System.out.println("No milestones for exam: "+exam.getName().toUpperCase()+"\n");
                        }else{
                            boolean printed = false;
                            Iterator<Milestone>ms = exam.getMs().iterator();
                            while(ms.hasNext()){
                                Milestone temp = ms.next();
                                if(temp.getType().equals(Milestone.Type.EXAM)){
                                    System.out.println(temp+"\n");
                                    printed = true;
                                    System.out.println("Study activities: ");
                                    if(temp.getSa().isEmpty()){
                                        System.out.println("No study activities for study task: "+temp.getName().toUpperCase()+"\n");
                                    }else{
                                        Iterator<Study_Activity>sa = temp.getSa().iterator();
                                        while(sa.hasNext()){
                                            System.out.println(sa.next()+"\n");
                                        }
                                    }
                                }
                            }
                            if(!printed)
                                System.out.println("No milestones for exam: "+exam.getName().toUpperCase()+"\n");
                        }*/
                        
                        System.out.println("Study tasks: ");
                        if(exam.getSt().isEmpty()){
                            System.out.println("No study tasks for exam: "+exam.getName().toUpperCase()+"\n");
                        }else{
                            Iterator<Study_Task>st = exam.getSt().iterator();
                            while(st.hasNext()){
                                Study_Task temp = st.next();
                                System.out.println(temp+"\n");
                                System.out.println("Study activities: ");
                                if(temp.getSa().isEmpty()){
                                    System.out.println("No study activities for study task: "+temp.getName().toUpperCase()+"\n");
                                }else{
                                    Iterator<Study_Activity>sa = temp.getSa().iterator();
                                    while(sa.hasNext()){
                                        System.out.println(sa.next()+"\n");
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "3":
                break;
        }
    }
    
    //If student has got a SDF and hasnt't uploaded it, then load data from it and update student's detail, and then edit studetndata.txt
    public static void Student_UploadSDF(Students student) throws ParseException{
        //Case when student has got a file and haven't uploaded it
        if(student.getFiles()!=null){
            System.out.println("You have these files available to upload: ");
            for(File a : student.getFiles()){
                System.out.print(a.getName()+"     ");
            }
            System.out.println("\nPlease enter a file name to upload: ");
            String spcae = scan.nextLine();
            String name = scan.nextLine();
            boolean found = false;
            for(File a : student.getFiles()){
                if(a.getName().equals(name)){
                    found = true;
                    student.uploadSemasterDataFile(a);
                    System.out.println("Uploaded successfully");
                    System.out.println("\n"+student);
                }
            }
            if(!found){
                System.out.println("Please make sure you have entered the file name correctly");
            }
         //Case when student hasn't got any files
        }else if(student.getFiles()==null){
            System.out.println("No files available to upload, please get it from the hub first...");
            Student_SelectFunctions(student);
        }
    }

    //Call the hub function and ask for a SDF
    public static void Student_getSDF(Students student){
        System.out.println("Now getting the semaster data file from hub...");
        student.setFiles(Hub.getSDF(student.number));
        //Case when student has received at least one file
        if(student.getFiles().length > 0){
            System.out.println("You have received "+student.getFiles().length+" file(s) from the hub successfully");
        }
        //Case when student has not received any file
        else
            System.out.println("There is no semaster data to receive from the hub, please contact them");
    }
    
    //If student has applied extension before, then a task's deadline can be delayed for a week by applying an extension
    public static void Student_ApplyExtension(Students student) throws ParseException{
        Calendar cal = Calendar.getInstance(); //Get today's day

        //Print out student's module
        System.out.println("You are taking these modules: ");
        Iterator<Module>module = student.getModule().iterator();
        while(module.hasNext()){
            System.out.print(module.next().getName().toUpperCase()+"     ");
        }

        System.out.println("\nEnter module name: ");
        String temp = scan.nextLine();
        String a = scan.nextLine();

        Module m = student.findStudentModule(a);

        //Case when Module m has been found from student's list and it has at least one task or exam
        if(m!=null && !m.getTask().isEmpty() && !m.getExam().isEmpty()){
            System.out.println("Module: "+m.getName()+" has these tasks and exams: ");

            //Print out module's task
            Iterator<Task>task = m.getTask().iterator();
            while(task.hasNext()){
                System.out.print(task.next().getName().toUpperCase()+"     ");
            }
            
            //Print out module's task
            Iterator<Exam>exam = m.getExam().iterator();
            while(exam.hasNext()){
                System.out.print(exam.next().getName().toUpperCase()+"     ");
            }

            System.out.println("\nEnter task/exam name: ");
            String b = scan.nextLine();

            Exam e = student.findStudentModuleExam(m, b);
            Task t = student.findStudentModuleTasks(m, b);
            
            if(e!=null && e.getDate().compareTo(cal.getTime()) >= 0){
                student.applyExtension(m, t, e);
                System.out.println("\nExtension has been granted");
            }else if(e!=null && e.getDate().compareTo(cal.getTime())<0){
                System.out.println("\nThis exam has already finished");
            }else if (t!=null && t.getEndDate().compareTo(cal.getTime()) >= 0 && t.getStartDate().compareTo(cal.getTime()) <=0){
                student.applyExtension(m, t, e);
                System.out.println("\nExtension has been granted");
            }
            //Case if Task t has been found but it has not been started yet
            else if(t!=null && t.getStartDate().compareTo(cal.getTime()) > 0 && t.getEndDate().compareTo(cal.getTime()) > 0){
                System.out.println("\nExtension for a task cannot be applied before the task start day");
            }
            //Case if Task t has been found but the deadline has already passed
            else if(t!=null && t.getStartDate().compareTo(cal.getTime()) < 0 && t.getEndDate().compareTo(cal.getTime()) <0)
                System.out.println("\nThe deadline of this task has passed already");
            else if (t== null && e == null)
                System.out.println("\nPlease make sure you have entered data correctly");
        }
        //Case when Module m has been found but does not contain any tasks
        else if(m!=null && m.getTask().isEmpty())
            System.out.println("Module: "+m.getName().toUpperCase()+" does not have any task");
        //Case when Module m not found
        else
            System.out.println("Please make sure you have entered the correct module name...");
    }
}