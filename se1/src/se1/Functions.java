package se1;

import java.util.*;
import java.io.*;
import java.text.*;

public class Functions{
    
    private final static Scanner scan = new Scanner(System.in);
    private static Database user;
    private static int failCount = 0; //Program will be terminated after 3 consecutive login failures
    
    //Load data from text file to initialize those arraylists 
    public static void LoadData() throws ParseException{
        try{
            //Clear some lists first... Data needs to be loaded again after the ForgotPw function
            Database.users.clear();
            
            //Read the module.txt and initialize school data
            readModuleData();
            
            //After all data have been read, read the user data at last
            FileReader fr = new FileReader(Database.getUserFile()); 
            try (BufferedReader br = new BufferedReader(fr)) {
                if(br.ready()){
                    for(String temp = br.readLine(); temp != null; temp = br.readLine()){
                        String[]data =temp.split(";"); //Used the semicolon as delimiter
                        switch (data[0]) {
                            //Can have more account types in the future
                            case "STUDENT":
                                Students student = new Students(data[2],data[3],data[1],data[4],Database.accType.STUDENT);
                                Database.users.add(student); //update database
                                break;
                        }
                    }
                }
            }
        }catch(IOException | NumberFormatException e){
        }
    }
    
    //Function to load data from studentsdata.txt
    public static void readStudentData(Students student){
        try{
            File f = Database.getStudentFile();
            FileReader fr = new FileReader(f);
            try (BufferedReader br = new BufferedReader(fr)) {
                if(br.ready()){
                    for(String temp = br.readLine(); temp!=null; temp = br.readLine()){
                        String[]data = temp.split(";");
                        
                        //Case when student has been found, initialize all student data
                        if(data[0].equals(student.number)){
                            student.setUpload(true); //Claim that student has already uploaded a SDF
                            student.setSchool(data[1].toUpperCase());
                            student.setCourse(data[2].toUpperCase());
                            student.setYear(data[3]);
                            student.setSemStartDate(Database.df.parse(data[4]));
                            student.setSemEndDate(Database.df.parse(data[5]));
                            
                            String[]mm = data[6].split(",");
                            for(String a: mm){
                                //Find module from database
                                Module m = Database.findModule(a);
                                if(m !=null){
                                    student.getModule().add(m); //Load module into student's module list
                                    student.getTask().addAll(m.getTask()); //Load module's task list into student's task list
                                    student.getExam().addAll(m.getExam());
                                }
                            }  
                            
                            if(!data[7].equals("no"))
                                readMilestoneData(student);
                            
                            if(!data[8].equals("no"))
                                readStData(student);
                            
                            if(!data[9].equals("no"))
                                readSaData(student);
                            
                            student.setCount(Integer.parseInt(data[10])); //Has student applied for an extension?
                            
                            //Case when the student has been granted an extension for a task/exam
                            if(Integer.parseInt(data[10]) != 0){
                                String [] modules = data[11].split(",");
                                String [] extendeds = data[12].split(",");
                                String [] dates = data[13].split(",");
                                for(int q = 0; q < modules.length; q++){
                                    Module m = student.findStudentModule(modules[q]);
                                    Task t = student.findStudentModuleTasks(m, extendeds[q]);
                                    Exam e = student.findStudentModuleExam(m, extendeds[q]);
                                    
                                    if(t!=null && e==null)
                                        t.updateEndDate(Database.df.parse(dates[q]));
                                    else if(t==null && e !=null)
                                        e.setDate(Database.df.parse(dates[q]));
                                }
                            }
                        }
                    }
                }
                br.close();
            }
        }catch(IOException | ParseException e){
        }
    }
    
    //Load data from modules.txt
    public static void readModuleData(){
        try{
            File f = Database.getModuleFile();
            FileReader fr = new FileReader(f);
            try (BufferedReader br = new BufferedReader(fr)) {
                if(br.ready()){
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        
                        //Create new module
                        Module m = new Module(data[0], data[1], Integer.parseInt(data[2]), Database.df.parse(data[3]), Database.df.parse(data[4]));
                        //Create newsfeed for this module
                        m.createNewsFeed(data[7]);
                        
                        //Case if this module has at least one task
                        if(!data[5].equals("no")){
                            String [] tk = data[5].split(",");
                            for(String a1: tk){
                                m = readTaskData(a1,m); //Call readTaskData function to keep loading data
                            }
                        }
                        
                        if(!data[6].equals("no")){
                            String tk [] = data[6].split(",");
                            for(String a1:tk){
                                m = readExamData(a1,m);
                            }
                        }
                        
                        if(Database.findModule(m.getName()) == null)
                            Database.modules.add(m);//Update database
                    }
                }
                br.close();
            }
        }catch(IOException | NumberFormatException | ParseException e){
        }
    }
    
    //Load data from task.txt
    public static Module readTaskData(String a, Module m){
        try{
            File f = Database.getTaskFile();
            FileReader fr = new FileReader(f);
            try (BufferedReader br = new BufferedReader(fr)) {
                if(br.ready()){
                    String str;
                    while((str = br.readLine())!=null){
                        String[]data = str.split(";");
                        if(data[0].toUpperCase().equals(m.getName().toUpperCase()) && data[1].toUpperCase().equals(a.toUpperCase())){
                            //Create new task
                            Task t = new Task(data[1], Database.df.parse(data[2]),Database.df.parse(data[3]));
                            if(data.length == 5)
                                t.setDescription(data[4]);
                            if(Database.findTask(t.getName()) == null)
                                Database.tasks.add(t); //Update database
                            if(m.findTask(t.getName()) == null)
                                m.getTask().add(t); //Add this task to module's task list
                            return m;
                        }
                    }
                }
                br.close();
            }
        }catch(IOException | ParseException e){
        }
        return m;
    }
    
    //Load data from exam.txt
    public static Module readExamData(String a, Module m){
        try{
            File f = Database.getExamFile();
            FileReader fr = new FileReader(f);
            try (BufferedReader br = new BufferedReader(fr)) {
                if(br.ready()){
                    String str;
                    while((str = br.readLine())!=null){
                        String[]data = str.split(";");
                        if(data[0].toUpperCase().equals(m.getName().toUpperCase()) && data[1].toUpperCase().equals(a.toUpperCase())){
                            //Create new exam
                            Exam e = new Exam(data[1], Database.df.parse(data[2]),Integer.parseInt(data[3]));
                            if(Database.findExam(e.getName()) == null)
                                Database.exams.add(e); //Update database
                            m.getExam().add(e); //Add this task to module's task list
                            return m;
                        }
                    }
                }
                br.close();
            }
        }catch(IOException | ParseException e){
        }
        return m;
    }
    
    //Load data from milestonedata.txt
    public static Students readMilestoneData(Students a){
        try{
            File f = Database.getMsFile();
            FileReader fr = new FileReader(f);
            try (BufferedReader br = new BufferedReader(fr)) {
                if(br.ready()){
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        if(data[0].equals(a.number)){
                            Module m = a.findStudentModule(data[1]);
                            if(data[2].equals("EXAM")){
                                Exam e = a.findStudentModuleExam(m, data[3]);
                                Milestone ms = new Milestone(data[4], Database.df.parse(data[5]), Database.df.parse(data[6]), Milestone.Type.EXAM);
                                e.setMs(ms);
                                ms.setDescription(data[7]);
                                a.getMilestone().add(ms);
                            }else if(data[2].equals("TASK")){
                                Task t = a.findStudentModuleTasks(m, data[3]);
                                Milestone ms = new Milestone(data[4], Database.df.parse(data[5]), Database.df.parse(data[6]), Milestone.Type.TASK);
                                t.setMilestone(ms);
                                ms.setDescription(data[7]);
                                a.getMilestone().add(ms);
                            }
                        }
                    }
                }
                br.close();
            }
        }catch(IOException | NumberFormatException | ParseException e){
        }
        return a;
    }
    
    //Load data from milestonedata.txt
    public static Students readStData(Students a) throws ParseException{
        try{
            File f = Database.getStFile();
            FileReader fr = new FileReader(f);
            try (BufferedReader br = new BufferedReader(fr)) {
                if(br.ready()){
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        if(data[0].equals(a.number)){
                            Module m = a.findStudentModule(data[1]);
                            
                            Study_Task.Type type;
                            if(data[6].toUpperCase().equals("PROGRAMMING"))
                                type = Study_Task.Type.PROGRAMMING;
                            else if(data[6].toUpperCase().equals("READING"))
                                type = Study_Task.Type.READING;
                            else if(data[6].toUpperCase().equals("WRITING"))
                                type = Study_Task.Type.WRITING;
                            else
                                type = Study_Task.Type.STUDYING;
                            
                            boolean result = data[10].equals("true");
                            
                            if(data[2].equals("EXAM")){
                                Exam e = a.findStudentModuleExam(m, data[3]);
                                Study_Task st = new Study_Task(data[4], Study_Task.Type1.EXAM, type, Double.parseDouble(data[5]), 
                                        Database.df.parse(data[7]), Database.df.parse(data[8]), result);
                                st.setDescription(data[9]);
                                e.getSt().add(st);
                                a.getSt().add(st);
                            }else if(data[2].equals("TASK")){
                                Task t = a.findStudentModuleTasks(m, data[3]);
                                Study_Task st = new Study_Task(data[4], Study_Task.Type1.TASK, type, Double.parseDouble(data[5])
                                        ,Database.df.parse(data[7]), Database.df.parse(data[8]), result);
                                st.setDescription(data[9]);
                                t.getSt().add(st);
                                a.getSt().add(st);
                            }
                        }
                    }
                }
                br.close();
            }
        }catch(IOException | NumberFormatException e){
        }
        return a;
    }
    
    //Load data from milestonedata.txt
    public static Students readSaData(Students a){
        try{
            File f = Database.getSaFile();
            FileReader fr = new FileReader(f);
            try (BufferedReader br = new BufferedReader(fr)) {
                if(br.ready()){
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        if(data[0].equals(a.number)){
                            Study_Activity sa = new Study_Activity(data[1], Double.parseDouble(data[2]));
                            sa.setDescription(data[3]);
                            
                            String [] temp = data[4].split(",");
                            for(String aa : temp){
                                Study_Task st = a.findStudentSt(aa);
                                Milestone ms = a.findStudentMS(aa);
                                if(st != null && ms == null){
                                    st.getSa().add(sa);
                                    st.setDuration(Double.parseDouble(data[2]));
                                }else if(st==null && ms!=null){
                                    ms.getSa().add(sa);
                                    ms.setDuration(Double.parseDouble(data[2]));
                                }
                            }
                            a.getSa().add(sa);
                        }
                    }
                }
                br.close();
            }
        }catch(IOException | NumberFormatException e){
        }
        return a;
    }
    
    //Start function, user can decide to login or create a new account
    public static void Start() throws ParseException{
        try{
            System.out.println("Press 1 to login, 2 to create new account, or 3 to close the program...");
            String a = scan.nextLine(); //Java have a stronger type check therefore it is a string instead of integer
            
            //Check input
            while(!a.equals("1") && !a.equals("2") && !a.equals("3")){
                System.out.println("Invalid input, please enter again...");
                a = scan.nextLine();
            }
            
            if(a.equals("1"))
                Login();
            else if(a.equals("2"))
                CreateUser();
            else
                System.exit(0);
        }catch(Exception e){
        }
    }
    
    //User can choose to create which type of account
    public static void CreateUser() throws ParseException{
        System.out.println("\nEnter 1 to create a student account, or 2 to go back: ");
        String a = scan.next();
        
        //Check input
        while(!a.equals("1") && !a.equals("2")){
            System.out.println("Invalid input, please enter again...");
            a = scan.next();
        }
        
        switch(a){
            case "1":
                CreateStudent();
                break;
            case "2":
                Start();
                break;
        }
    }
    
    //Ask user to enter required data for creating a student account
    public static void CreateStudent() throws ParseException{
        try{
            FileOutputStream fos = new FileOutputStream(Database.getUserFile(), true); //The way to open the file output stream
            String a,b,c,d;
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))){
                //Ask for details...
                System.out.println("Enter account name: ");
                String space = scan.nextLine();
                a = scan.nextLine(); 
                System.out.println("Enter password: ");
                b = scan.nextLine();
                System.out.println("Enter student number: ");
                c = scan.nextLine();
                System.out.println("Enter email: ");
                d = scan.nextLine();
                
                //Data validation...
                if(!checkNumber(c) && !checkEmail(d)){
                    System.out.println("Invalid email format and student number, please try again");
                    CreateUser();
                }else if(!checkEmail(d)){
                    System.out.println("Invalid email format, please try again");
                    CreateUser();
                }else if(!checkNumber(c)){
                    System.out.println("Invalid student number, please try again");
                    CreateUser();
                }else
                    checkDatabase(a,c,d);
                
                //Write these data into the text file
                bw.write(Database.accType.STUDENT.toString()+";"+c+";"+a+";"+b+";"+d);
                bw.newLine();
                bw.close();
            }
            
            //Since the text file won't be read again before restarting the program, therefore add this new student to the arraylist
            Students temp = new Students(a,b,c,d,Database.accType.STUDENT);
            Database.users.add(temp);
            
            System.out.println("\nCreated, now back to login page\n");
            Start();
        }catch(IOException | ParseException e){
        }
    }
    
    public static boolean checkNumber(String a){
        return (a.length() == 9 && a.matches("\\d+") && a.charAt(0)== '1');
    }
    
    //Return true if the id contains 8 character and matches a particular format
    public static boolean checkId(String a){
        String [] temp = a.split("");
        if(temp.length != 8)
            return false;
        else{
            for(int q = 0; q< temp.length; q++){
                if(q <3 || q>4){
                    if(!temp[q].matches(".*[a-z].*"))
                        return false;
                }else{
                    if(!temp[q].matches("\\d+"))
                        return false;
                }
            }
        }
        return true;
    }
    
    //Return true if startdate is not equals to or greater than end date, and also within range if tempSd and tempEd are not null
    public static boolean checkDate(Date tempSd, Date tempEd, Date sd, Date ed){
        if(sd.compareTo(ed) >= 0){
            System.out.println("Start date can not be greater than or equal to end date");
            return false;
        }else if(tempSd != null && tempEd != null && tempSd.compareTo(sd) <=0 && tempEd.compareTo(ed) > 0){
            return true;
        }else if(tempSd == null && tempEd == null && sd.compareTo(ed) < 0){
            return true;
        }else
            System.out.println("Start date and end date must be within the range");
        return false;
    }
    
    //Return true if email contains a "@" and at least one "." after "@"
    public static boolean checkEmail(String a){
        if(a.contains("@")){
            String [] parts = a.split("@");
            String [] data = parts[1].split("\\.");
            if(parts.length ==2 && parts[1].contains(".") && !data[0].trim().equals(""))
                return true;
        }
        return false;
    }
    
    //Make sure there is no duplication of account number, id, and email in the database
    public static void checkDatabase(String a, String c, String d) throws ParseException{
        boolean error = false;
        System.out.println();
        Iterator<Database>it = Database.users.iterator();
        while(it.hasNext()){
            Database temp = it.next();
            if(temp.name.equals(a)){
                System.out.println("Account name is already existed in database");
                error = true;
            }
            if(temp.email.equals(d)){
                System.out.println("Email is already existed in database");
                error = true;
            }
            if(temp.number.equals(c)){
                System.out.println("Number is already existed in database");
                error = true;
            }
        }
        if(error)
            CreateUser();
    }
    
    //Login function, loop through the database to look for matches, program will be terminated after 3 consecutive login failure
    public static void Login() throws ParseException{
        try{
            System.out.println("Please enter account name: ");
            String b = scan.nextLine();
            
            System.out.println("Please enter password: ");
            String c = scan.nextLine();

            //Loop through the user list and look for matches
            Boolean found = false;
            Iterator<Database>it = Database.users.iterator();
            while(it.hasNext()){
                Database temp = it.next();
                if(temp.name.equals(b) && temp.pw.equals(c)){
                    found = true;
                    user = temp; //User found
                }
            }

            if(found == true){
                System.out.println("\nWelcome back, ");
                Function(); //Move on...
            }else{
                System.out.println("Incorrect account number or password, please try again\n");
                failCount++;
                if(failCount == 3){
                    forgotPw();
                }else
                    Start();
            }
        }catch(InputMismatchException e){
            System.out.println("Invalid input type");
        }
    }
    
    //If 3 consecutive login failure.....
    public static void forgotPw() throws ParseException{
        System.out.println("\nYou have entered the wrong password for too many times...");
        System.out.println("Enter 0 to go back, or 1 to reset password: ");
        String a = scan.nextLine();
        
        //check input
        while(!a.equals("0") && !a.equals("1")){
            System.out.println("Invalid input, please retry: ");
            a = scan.nextLine();
        }
        
        //Reset fail count and continue to login
        if(a.equals("0")){
            failCount = 0;
            Start();
        }
        //Ask for account number, id, and email to find matches from database
        else{
            System.out.println("\nPlease answer a few questions before resetting the password...");
            System.out.println("What is your account name? ");
            String name = scan.nextLine();
            System.out.println("What is your account email address?");
            String email = scan.nextLine();
            System.out.println("What is your account numner?");
            String number = scan.nextLine();
            
            boolean found = false;
            //Loop through the database
            Iterator<Database>it = Database.users.iterator();
            while(it.hasNext() && !found){
                Database temp = it.next();
                
                //Case if matches has been found
                if(temp.name.equals(name) && temp.number.equals(number)
                        && temp.email.toUpperCase().equals(email.toUpperCase())){
                    user = temp;
                    found = true;
                }
            }
            
            //Shut down program if no account has been found
            if(!found){
                System.out.println("\nCannot find your account in the database...goodbye");
                System.exit(0);
            }
            //Call the updatePw function to reset password, then call the loaddata function again to upate database
            else{
                System.out.println("\nVerified... Now you are free to update the password");
                UpdatePw(user);
                System.out.println();
                LoadData();
                Start();
            }
        }
    }
    
    //Show the functions base on user's account type
    public static void Function() throws ParseException{
        if(user.ac.equals(Database.accType.STUDENT)){
            Students a = (Students) user;
            readStudentData(a); //Load data from studentsdata.txt if it is a student account
            System.out.println(a); //Print user detail
            Student_Functions.Student_SelectFunctions(a);
        }
    }
    
    //Function to update password
    public static void UpdatePw(Database user){
        //New pw
        System.out.println("Enter new password: ");
        String a = scan.next();

        //Double check
        System.out.println("Enter new password again: ");
        String b = scan.next();

        if(!a.equals(b))
            System.out.println("Please make sure you have entered the new password correctly...");
        else{
            editUserdata(user,a,0); //edit text file
            System.out.println("Password has been updated successfully");
        }
    }
    
    public static void UpdateEmail(Database user) throws ParseException{
        //New email
        System.out.println("Enter new email address: ");
        String a = scan.next();

        //Double check
        System.out.println("Enter new email address again: ");
        String b = scan.next();

        if(!a.equals(b))
            System.out.println("Please make sure you have entered the new email address correctly...");
        else{
            editUserdata(user, b, 1); //edit text file
            user.setEmail(a);
            System.out.println("Email has been updated successfully");
        }
    }
    
    //Edit users.txt base on the value of c
    public static void editUserdata(Database a, String b , int c){
        try{
            File temp = File.createTempFile("file", ".txt", Database.getUserFile().getParentFile());
            FileOutputStream fos = new FileOutputStream(temp);
            
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                FileReader fr = new FileReader(Database.getUserFile());
                try (BufferedReader br = new BufferedReader(fr)) {
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        //If user has been found
                        if(data[2].equals(a.name)){
                            switch(c){
                                case 0:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3].replaceAll(a.pw, b)+";"+data[4]);
                                    break;
                                case 1:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4].replaceAll(a.email, b));
                                    break;
                            }
                        }else
                            bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]);
                        bw.newLine();
                    }
                    br.close();
                }
                bw.close();
                Database.getUserFile().delete();
                temp.renameTo(Database.getUserFile());
            }
        }catch(Exception e){
        }
    }
    
    //Edit the milestonedata.txt base on the value of c
    public static void writeMilestone(Milestone a, String b){
        try{
            File f = Database.getMsFile();
            FileOutputStream fos = new FileOutputStream(f,true);
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                bw.write(b);
                bw.newLine();
                bw.close();
            }
        }catch(Exception e){
        }
    }
    
    //Edit the milestonedata.txt base on the value of c
    public static void writeST(Study_Task a, String b){
        try{
            File f = Database.getStFile();
            FileOutputStream fos = new FileOutputStream(f,true);
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                bw.write(b);
                bw.newLine();
                bw.close();
            }
        }catch(Exception e){
        }
    }
    
    //Edit the milestonedata.txt base on the value of c
    public static void writeSA(Study_Activity a, String b){
        try{
            File f = Database.getSaFile();
            FileOutputStream fos = new FileOutputStream(f,true);
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                bw.write(b);
                bw.newLine();
                bw.close();
            }
        }catch(Exception e){
        }
    }
    
    //Edit student data base on the value of c
    public static void editStudentdata(Students student, String b, int c){
        try{
            File f = Database.getStudentFile();
            File temp = File.createTempFile("file", ".txt", f.getParentFile());
            FileOutputStream fos = new FileOutputStream(temp);
            
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                FileReader fr = new FileReader(f);
                try (BufferedReader br = new BufferedReader(fr)) {
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        switch(c){
                            case 0:
                                if(data[0].equals(student.number) && Integer.parseInt(data[10])>0)
                                    if(data[7].equals("no"))
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                                +data[7].replaceAll(data[7], b)+";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                    else
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                                +data[7]+","+b+";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else if(data[0].equals(student.number) && Integer.parseInt(data[10])==0)
                                    if(data[7].equals("no"))
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                            +data[7].replaceAll(data[7], b)+";"+data[8]+";"+data[9]+";"+data[10]);
                                    else
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                            +data[7]+","+b+";"+data[8]+";"+data[9]+";"+data[10]);
                                else if(Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                            +";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                            +";"+data[8]+";"+data[9]+";"+data[10]);
                                bw.newLine();
                                break;
                            case 1:
                                if(data[0].equals(student.number) && Integer.parseInt(data[10])>0)
                                    if(data[8].equals("no"))
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                                +data[7]+";"+data[8].replaceAll(data[8], b)+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                    else
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                                +data[7]+";"+data[8]+","+b+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else if(data[0].equals(student.number) && Integer.parseInt(data[10])==0)
                                    if(data[8].equals("no"))
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                            +data[7]+";"+data[8].replaceAll(data[8], b)+";"+data[9]+";"+data[10]);
                                    else
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                            +data[7]+";"+data[8]+","+b+";"+data[9]+";"+data[10]);
                                else if(Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                            +";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                            +";"+data[8]+";"+data[9]+";"+data[10]);
                                bw.newLine();
                                break;
                            case 2:
                                if(data[0].equals(student.number) && Integer.parseInt(data[10])>0)
                                    if(data[9].equals("no"))
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                                +data[7]+";"+data[8]+";"+data[9].replaceAll(data[9], b)+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                    else
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                                +data[7]+";"+data[8]+";"+data[9]+","+b+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else if(data[0].equals(student.number) && Integer.parseInt(data[10])==0)
                                    if(data[9].equals("no"))
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                            +data[7]+";"+data[8]+";"+data[9].replaceAll(data[9], b)+";"+data[10]);
                                    else
                                        bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                            +data[7]+";"+data[8]+";"+data[9]+","+b+";"+data[10]);
                                else if(Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                            +";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                            +";"+data[8]+";"+data[9]+";"+data[10]);
                                bw.newLine();
                                break;
                            case 3:
                                String [] s = b.split(";");
                                
                                if(data[0].equals(student.number) && Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                        +data[7].replaceAll(s[1], s[0])+";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else if(data[0].equals(student.number) && Integer.parseInt(data[10])==0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                        +data[7].replaceAll(s[1], s[0])+";"+data[8]+";"+data[9]+";"+data[10]);
                                else if(Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                        +";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                        +";"+data[8]+";"+data[9]+";"+data[10]);
                                bw.newLine();
                                break;
                            case 4:
                                String [] ss = data[7].toUpperCase().split(",");
                                
                                List<String>list = new ArrayList<>();
                                Collections.addAll(list, ss);
                                list.remove(b.toUpperCase());
                                
                                String newdata = "";
                                if(list.isEmpty())
                                    newdata = "no";
                                else{
                                    ss = list.toArray(new String[list.size()]);
                                    for(int count = 0;count < ss.length; count++)
                                        newdata += ss[count] += ",";
                                    newdata = newdata.substring(0,newdata.length()-1);
                                }
                                
                                if(data[0].equals(student.number) && Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                        +newdata+";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else if(data[0].equals(student.number) && Integer.parseInt(data[10])==0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                        +newdata+";"+data[8]+";"+data[9]+";"+data[10]);
                                else if(Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                        +";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                        +";"+data[8]+";"+data[9]+";"+data[10]);
                                bw.newLine();
                                break;
                            case 5:
                                String [] a = b.split(";");
                                
                                if(data[0].equals(student.number) && Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                        +data[7]+";"+data[8].replaceAll(a[1], a[0])+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else if(data[0].equals(student.number) && Integer.parseInt(data[10])==0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                        +data[7]+";"+data[8].replaceAll(a[1], a[0])+";"+data[9]+";"+data[10]);
                                else if(Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                        +";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                        +";"+data[8]+";"+data[9]+";"+data[10]);
                                bw.newLine();
                                break;
                            case 6:
                                String [] sss = data[8].toUpperCase().split(",");
                                
                                List<String>listt = new ArrayList<>();
                                Collections.addAll(listt, sss);
                                listt.remove(b.toUpperCase());
                                
                                String newdataa = "";
                                if(listt.isEmpty())
                                    newdata = "no";
                                else{
                                    sss = listt.toArray(new String[listt.size()]);
                                    for(int count = 0;count < sss.length; count++)
                                        newdataa += sss[count] += ",";
                                    newdata = newdataa.substring(0,newdataa.length()-1);
                                }
                                
                                if(data[0].equals(student.number) && Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                        +data[7]+";"+newdata+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else if(data[0].equals(student.number) && Integer.parseInt(data[10])==0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"
                                        +data[7]+";"+newdata+";"+data[9]+";"+data[10]);
                                else if(Integer.parseInt(data[10])>0)
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                        +";"+data[8]+";"+data[9]+";"+data[10]+";"+data[11]+";"+data[12]+";"+data[13]);
                                else
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"+data[6]+";"+data[7]
                                        +";"+data[8]+";"+data[9]+";"+data[10]);
                                bw.newLine();
                                break;
                        }
                    }
                    br.close();
                }
                bw.close();
                f.delete();
                temp.renameTo(f);
            }
        }catch(IOException | NumberFormatException e){
        }
    }
    
    //Edit the milestonedata.txt base on the value of c
    public static void editMilestonedata(Milestone a, String b, int c, Students d){
        try{
            File f = Database.getMsFile();
            File temp = File.createTempFile("file", ".txt", f.getParentFile());
            FileOutputStream fos = new FileOutputStream(temp);
            
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                FileReader fr = new FileReader(f);
                try (BufferedReader br = new BufferedReader(fr)) {
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        if(data[0].equals(d.number) && data[4].toUpperCase().equals(a.getName().toUpperCase())){
                            switch(c){
                                case 0:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4].replaceAll(data[4], b)+";"+data[5]+";"
                                            +data[6]+";"+data[7]);
                                    bw.newLine();
                                    break;
                                case 1:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5].replaceAll(data[5], b)+";"
                                            +data[6]+";"+data[7]);
                                    bw.newLine();
                                    break;
                                case 2:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"
                                            +data[6].replaceAll(data[6], b)+";"+data[7]);
                                    bw.newLine();
                                    break;
                                case 3:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"
                                            +data[6]+";"+data[7].replaceAll(data[7], b));
                                    bw.newLine();
                                    break;
                                case 4:
                                    break;
                            }
                        }else{
                            bw.write(str);
                            bw.newLine();
                        }
                    }
                    br.close();
                }
                bw.close();
                f.delete();
                temp.renameTo(f);
            }
        }catch(Exception e){
        }
    }
    
     //Edit the milestonedata.txt base on the value of c
    public static void editStdata(Study_Task a, String b, int c, Students d){
        try{
            File f = Database.getStFile();
            File temp = File.createTempFile("file", ".txt", f.getParentFile());
            FileOutputStream fos = new FileOutputStream(temp);
            
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"))) {
                FileReader fr = new FileReader(f);
                try (BufferedReader br = new BufferedReader(fr)) {
                    String str;
                    while((str=br.readLine())!=null){
                        String[]data = str.split(";");
                        if(data[0].equals(d.number) && data[4].toUpperCase().equals(a.getName().toUpperCase())){
                            switch(c){
                                case 0:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4].replaceAll(data[4], b)+";"+data[5]+";"
                                            +data[6]+";"+data[7]+";"+data[8]+";"+data[9]+";"+data[10]);
                                    bw.newLine();
                                    break;
                                case 1:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"
                                            +data[6]+";"+data[7].replaceAll(data[7], b)+";"+data[8]+";"+data[9]+";"+data[10]);
                                    bw.newLine();
                                    break;
                                case 2:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"
                                            +data[6]+";"+data[7]+";"+data[8].replaceAll(data[8], b)+";"+data[9]+";"+data[10]);
                                    bw.newLine();
                                    break;
                                case 3:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5]+";"
                                            +data[6]+";"+data[7]+";"+data[8]+";"+data[9].replaceAll(data[9], b)+";"+data[10]);
                                    bw.newLine();
                                    break;
                                case 4:
                                    break;
                                case 5:
                                    bw.write(data[0]+";"+data[1]+";"+data[2]+";"+data[3]+";"+data[4]+";"+data[5].replaceAll(data[5], b)+";"
                                            +data[6]+";"+data[7]+";"+data[8]+";"+data[9]+";"+data[10]);
                                    bw.newLine();
                                    break;
                            }
                        }else{
                            bw.write(str);
                            bw.newLine();
                        }
                    }
                    br.close();
                }
                bw.close();
                f.delete();
                temp.renameTo(f);
            }
        }catch(Exception e){
        }
    }
    
    public static void printModuleTaskExam(List<Module>m, List<Task>t, List<Exam>e, List<Study_Task>st, List<Milestone>ms){
        if(m != null && t == null && e == null && st ==null && ms == null){
        //Print modules
        Iterator<Module>it = m.iterator();
        int line = 0;
        boolean printed = false;
        while(it.hasNext()){
            printed = false;
            System.out.print(it.next().getName().toUpperCase()+"     ");
            line++;
            if(line%5 ==0){
                System.out.println();
                printed = true;
            }
        }
        if(!printed)
            System.out.println();
        }else if(m == null && t!=null && e ==null && st ==null && ms ==null){
            //Print out tasks
            Iterator<Task>list = t.iterator();
            int line = 0;
            boolean printed = false;
            while(list.hasNext()){
                printed = false;
                System.out.print(list.next().getName().toUpperCase()+"     ");
                line++;
                if(line%5 ==0){
                    System.out.println();
                    printed = true;
                }
            }
            if(!printed)
                System.out.println();
        }else if(m == null && t==null && e != null && st ==null && ms ==null){
            //Print out exams
            Iterator<Exam>list = e.iterator();
            int line = 0;
            boolean printed = false;
            while(list.hasNext()){
                printed = false;
                System.out.print(list.next().getName().toUpperCase()+"     ");
                line++;
                if(line%5 ==0){
                    System.out.println();
                    printed = true;
                }
            }
            if(!printed)
                System.out.println();
        }else if(m == null && t==null && e ==null && st !=null && ms ==null){
            //Print out study tasks
            Iterator<Study_Task>list = st.iterator();
            int line = 0;
            boolean printed = false;
            while(list.hasNext()){
                printed = false;
                Study_Task temp = list.next();
                System.out.print(temp.getName().toUpperCase()+"("+temp.getType()+") "+temp.getEstimeatedTimespent()+"hours     ");
                line++;
                if(line%5 ==0){
                    System.out.println();
                    printed = true;
                }
            }
            if(!printed)
                System.out.println();
        }else if(m == null && t==null && e ==null && st ==null && ms !=null){
            //Print out milestones
            Iterator<Milestone>list = ms.iterator();
            int line = 0;
            boolean printed = false;
            while(list.hasNext()){
                printed = false;
                System.out.print(list.next().getName().toUpperCase()+"     ");
                line++;
                if(line%5 ==0){
                    System.out.println();
                    printed = true;
                }
            }
            if(!printed)
                System.out.println();
        }
    }
}