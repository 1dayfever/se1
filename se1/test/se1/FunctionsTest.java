/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ftr15ssu
 */
public class FunctionsTest {
    
    public FunctionsTest() {
    }
    
        Hub ben = new Hub("Ben","123","ben@ben.com",Database.accType.STUDENT);
//  String name, String pw, String number, String email, accType ac
    List<Students>student = new ArrayList<>();
    Students benny = new Students("Ben","123","100124405","ben@ben.com",
            Database.accType.STUDENT);
    
    List<Module> list = new ArrayList<>();
    Date d = new Date(12,2,17);
    Date d2 = new Date(12,6,17);
    Module module = new Module("Programming","2017",23,d,d);
    Task ta = new Task("do something",d,d);
    Exam exam = new Exam("Programming", d2,89);
    

    /**
     * Test of LoadData method, of class Functions.
     */
    @Test
    public void testLoadData() throws Exception {
        System.out.println("LoadData");
        Functions.LoadData();
    }

    /**
     * Test of readStudentData method, of class Functions.
     */
    @Test
    public void testReadStudentData() {
        System.out.println("readStudentData");
        Students stu = benny;
        Functions.readStudentData(stu);
    }

    /**
     * Test of readModuleData method, of class Functions.
     */
    @Test
    public void testReadModuleData() {
        System.out.println("readModuleData");
        Functions.readModuleData();
    }

    /**
     * Test of readTaskData method, of class Functions.
     */
    @Test
    public void testReadTaskData() {
        System.out.println("readTaskData");
        String a = "programming";
        Module m = module;
        Module expResult = module;
        Module result = Functions.readTaskData(a, m);
        assertEquals(expResult, result);
    }

    /**
     * Test of readExamData method, of class Functions.
     */
    @Test
    public void testReadExamData() {
        System.out.println("readExamData");
        String a = "";
        Module m = module;
        Module expResult = module;
        Module result = Functions.readExamData(a, m);
        assertEquals(expResult, result);
    }

    /**
     * Test of readMilestoneData method, of class Functions.
     */
    @Test
    public void testReadMilestoneData() {
        System.out.println("readMilestoneData");
        Students a = benny;
        Students expResult = benny;
        Students result = Functions.readMilestoneData(a);
        assertEquals(expResult, result);
    }

    /**
     * Test of readStData method, of class Functions.
     */
    @Test
    public void testReadStData() throws Exception {
        System.out.println("readStData");
        Students a = benny;
        Students expResult = benny;
        Students result = Functions.readStData(a);
        assertEquals(expResult, result);
    }

    /**
     * Test of readSaData method, of class Functions.
     */
    @Test
    public void testReadSaData() {
        System.out.println("readSaData");
        Students a = benny;
        Students expResult = benny;
        Students result = Functions.readSaData(a);
        assertEquals(expResult, result);
    }

   


    /**
     * Test of checkNumber method, of class Functions.
     */
    @Test
    public void testCheckNumber() {
        System.out.println("checkNumber");
        String a = "100124405";
        boolean expResult = true;
        boolean result = Functions.checkNumber(a);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkId method, of class Functions.
     */
    @Test
    public void testCheckId() {
        System.out.println("checkId");
        String a = "100124405";
        boolean expResult = false; //check this because there is no id by that 
                                   //number
        boolean result = Functions.checkId(a);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkDate method, of class Functions.
     */
    @Test
    public void testCheckDate() {
        System.out.println("checkDate");
        Date d1 = new Date(12,5,2017);
        Date d2 = new Date(13,5,2017);
        Date d3 = new Date(1,5,2017);
        Date d4 = new Date(20,5,2017);
        Date tempSd = null;
        Date tempEd = null;
        Date sd = d1;
        Date ed = d2;
        boolean expResult = true;
        boolean result = Functions.checkDate(tempSd, tempEd, sd, ed);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkEmail method, of class Functions.
     */
    @Test
    public void testCheckEmail() {
        System.out.println("checkEmail");
        String a = "ben@gmail.com";
        boolean expResult = true;
        boolean result = Functions.checkEmail(a);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkDatabase method, of class Functions.
     */
    @Test
    public void testCheckDatabase() throws Exception {
        System.out.println("checkDatabase");
        String a = "1";
        String c = "100124405";
        String d = "ben@gmail.com";
        Functions.checkDatabase(a, c, d);
    }





    /**
     * Test of editUserdata method, of class Functions.
     */
    @Test
    public void testEditUserdata() {
        System.out.println("editUserdata");
        Database a = ben;
        String b = "lksajf";
        int c = 10;
        Functions.editUserdata(a, b, c);
    }

    /**
     * Test of writeMilestone method, of class Functions.
     */
    @Test
    public void testWriteMilestone() {
        System.out.println("writeMilestone");
        Milestone a = null;
        String b = "";
        Functions.writeMilestone(a, b);
    }

    /**
     * Test of writeST method, of class Functions.
     */
    @Test
    public void testWriteST() {
        System.out.println("writeST");
        Study_Task a = null;
        String b = "";
        Functions.writeST(a, b);
    }

    /**
     * Test of writeSA method, of class Functions.
     */
    @Test
    public void testWriteSA() {
        System.out.println("writeSA");
        Study_Activity a = null;
        String b = "";
        Functions.writeSA(a, b);
    }

    /**
     * Test of editStudentdata method, of class Functions.
     */
    @Test
    public void testEditStudentdata() {
        System.out.println("editStudentdata");
        Students student = benny;
        String b = "";
        int c = 0;
        Functions.editStudentdata(student, b, c);
    }

    /**
     * Test of editMilestonedata method, of class Functions.
     */
    @Test
    public void testEditMilestonedata() {
        System.out.println("editMilestonedata");
        Milestone a = null;
        String b = "";
        int c = 0;
        Students d = null;
        Functions.editMilestonedata(a, b, c, d);
    }

    /**
     * Test of editStdata method, of class Functions.
     */
    @Test
    public void testEditStdata() {
        System.out.println("editStdata");
        Study_Task a = null;
        String b = "";
        int c = 0;
        Students d = null;
        Functions.editStdata(a, b, c, d);
    }

    /**
     * Test of printModuleTaskExam method, of class Functions.
     */
    @Test
    public void testPrintModuleTaskExam() {
        System.out.println("printModuleTaskExam");
        List<Module> m = null;
        List<Task> t = null;
        List<Exam> e = null;
        List<Study_Task> st = null;
        List<Milestone> ms = null;
        Functions.printModuleTaskExam(m, t, e, st, ms);
    }
    
}
