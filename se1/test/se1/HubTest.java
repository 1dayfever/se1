package se1;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class HubTest {
    
    public HubTest() {
    }
    
    Hub ben = new Hub("Ben","123","ben@ben.com",Database.accType.STUDENT);
//  String name, String pw, String number, String email, accType ac
    List<Students>student = new ArrayList<>();
    Students benny = new Students("Ben","123","100124405","ben@ben.com",
            Database.accType.STUDENT);
    
    List<Module> list = new ArrayList<>();
    Date d = new Date(12,2,17);
    Date d2 = new Date(12,6,17);
    Module module = new Module("Programming","2017",23,d,d);
    Task ta = new Task("do something",d,d);
    Exam exam = new Exam("Programming", d2,89);

    /**
     * Test of addAllModules method, of class Hub.
     */
    @Test
    public void testAddAllModules() {
        System.out.println("addAllModules");
        List<Module> a = list;
        Hub instance = ben;
        instance.addAllModules(a);
    }

    /**
     * Test of removeAllModules method, of class Hub.
     */
    @Test
    public void testRemoveAllModules() {
        System.out.println("removeAllModules");
        List<Module> a = list;
        Hub instance = ben;
        instance.removeAllModules(a);

    }


    /**
     * Test of findHubModule method, of class Hub.
     */
    @Test
    public void testFindHubModule() {
        System.out.println("findHubModule");
        String a = "programming";
        list.add(module);
        ben.addAllModules(list);
        Hub instance = ben;
        Module expResult = module;
        Module result = instance.findHubModule(a);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDeadline method, of class Hub.
     * @throws java.lang.Exception
     */
    @Test
    public void testSetDeadline() throws Exception {
        System.out.println("setDeadline");
        Students stu = benny;
        Module b = module;
        Task c = ta;
        Exam da = exam;
        Hub.setDeadline(stu, b, c, da);
    }

    /**
     * Test of updateDeadline method, of class Hub.
     */
    @Test
    public void testUpdateDeadline() {
        System.out.println("updateDeadline");
        Students stu = benny;
        String b = "";
        String c = "";
        Date da = d2;
        Hub.updateDeadline(stu, b, c, da);
    }




    
}
